<?php
/**
 * (c) Jan Thiemen <jan.thiemen@euroskano.nl>
 *
 * This source file is subject to the GNU General Public License (v3) that is bundled
 * with this source code in the file LICENSE.
 */

namespace Janthiemen\Erpnextphpapi;

require_once "ERPNextObject.php";
require_once "ERPNextException.php";
require_once "Supplier.php";
require_once "Customer.php";
require_once "SalesInvoice.php";
require_once "InvoiceItem.php";
require_once "Item.php";
require_once "PaymentEntryReference.php";
require_once "PaymentEntry.php";

use Exception;

class ERPNext {

	private $instance;
	private $host;
	private $auth_user;
	private $auth_token;
	private $verify_ssl;

	/**
	 * Constructor for the ERPNext API Wrapper
	 * @param string $instance The URL of your ERPNext instance (without https://)
	 * @param string $host The hostname of your ERPNext instance (without https://)
	 * @param string $auth_user The authentication user key from ERPNext
	 * @param string $auth_token The authentication token from ERPNext
	 * @param bool $verify_ssl Should the ssl certificate be verified?
	 */
	public function __construct($instance, $host, $auth_user, $auth_token, $verify_ssl = true) {
		$this->instance = $instance;
		$this->host = $host;
		$this->auth_user = $auth_user;
		$this->auth_token = $auth_token;
		$this->verify_ssl = $verify_ssl;
	}

	/**
	 * Create a new supplier.
	 * @param Customer function The supplier object containing the data to create
	 * @return Customer
	 * @throws ErpNextException
	 */
	public function create_customer(Customer $customer) {
		return new Customer($this->post_object("Customer", $customer)->data);
	}

	/**
	 * Create a new supplier.
	 * @param Supplier $supplier The supplier object containing the data to create
	 * @return Supplier
	 * @throws ErpNextException
	 */
	public function create_supplier(Supplier $supplier) {
		return new Supplier($this->post_object("Supplier", $supplier)->data);
	}

	/**
	 * Get a specific customer.
	 * @param string $key The key of the customer.
	 * @return Customer
	 * @throws ErpNextException
	 */
	public function get_customer($key) {
		return new Customer($this->get_object("Customer", $key));
	}

	/**
	 * Get a specific supplier.
	 * @param string $key The key of the supplier.
	 * @return Supplier
	 * @throws ErpNextException
	 */
	public function get_supplier($key) {
		return new Supplier($this->get_object("Supplier", $key)->data);
	}

	/**
	 * Get a list of suppliers.
	 * @param string $fields A string containing the fields to retrieve (e.g. ["name","first_name"])
	 * @param string $filters A string containing the filters (e.g. [["Person","first_name","=","Jane"]])
	 * @return \stdClass
	 * @throws ErpNextException
	 */
	public function get_suppliers($fields = "", $filters = "") {
		return $this->get_object_list("Supplier", $fields, $filters);
	}

	/**
	 * Get a specific sales invoice.
	 * @param string $key The key of the sales invoice.
	 * @return SalesInvoice
	 * @throws ErpNextException
	 */
	public function get_sales_invoice($key) {
		return new SalesInvoice($this->get_object("Sales%20Invoice", $key)->data);
	}

	/**
	 * Get a list of sales invoice.
	 * @param string $fields A string containing the fields to retrieve (e.g. ["name","first_name"])
	 * @param string $filters A string containing the filters (e.g. [["Person","first_name","=","Jane"]])
	 * @return \stdClass
	 * @throws ErpNextException
	 */
	public function get_sales_invoices($fields = "", $filters = "") {
		return $this->get_object_list("Sales%20Invoice", $fields, $filters);
	}

	/**
	 * Create a new sales invoice.
	 * @param SalesInvoice $invoice The sales invoice object containing the data to create
	 * @return SalesInvoice
	 * @throws ErpNextException
	 */
	public function create_sales_invoice(SalesInvoice $invoice) {
		return new SalesInvoice($this->post_object("Sales%20Invoice", $invoice)->data);
	}

	/**
	 * Get a specific Purchase invoice.
	 * @param string $key The key of the Purchase invoice.
	 * @return PurchaseInvoice
	 * @throws ErpNextException
	 */
	public function get_purchase_invoice($key) {
		return new PurchaseInvoice($this->get_object("Purchase%20Invoice", $key)->data);
	}

	/**
	 * Get a list of Purchase invoices.
	 * @param string $fields A string containing the fields to retrieve (e.g. ["name","first_name"])
	 * @param string $filters A string containing the filters (e.g. [["Person","first_name","=","Jane"]])
	 * @return \stdClass
	 * @throws ErpNextException
	 */
	public function get_purchase_invoices($fields = "", $filters = "") {
		return $this->get_object_list("Purchase%20Invoice", $fields, $filters);
	}

	/**
	 * Create a new Purchase invoice.
	 * @param PurchaseInvoice $invoice The Purchase invoice object containing the data to create
	 * @return PurchaseInvoice
	 * @throws ErpNextException
	 */
	public function create_purchase_invoice(PurchaseInvoice $invoice) {
		return new PurchaseInvoice($this->post_object("Purchase%20Invoice", $invoice)->data);
	}

	/**
	 * Get a specific Payment entry.
	 * @param string $key The key of the Payment entry.
	 * @return PaymentEntry
	 * @throws ErpNextException
	 */
	public function get_payment_entry($key) {
		return $this->get_object("Payment%20Entry", $key)->data;
//		return new PaymentEntry($this->get_object("Payment%20Entry", $key)->data);
	}

	/**
	 * Get a list of Payment entrys.
	 * @param string $fields A string containing the fields to retrieve (e.g. ["name","first_name"])
	 * @param string $filters A string containing the filters (e.g. [["Person","first_name","=","Jane"]])
	 * @return \stdClass
	 * @throws ErpNextException
	 */
	public function get_payment_entries($fields = "", $filters = "") {
		return $this->get_object_list("Payment%20Entry", $fields, $filters);
	}

	/**
	 * Create a new Payment entry.
	 * @param PaymentEntry $entry The Payment entry object containing the data to create
	 * @return PaymentEntry
	 * @throws ErpNextException
	 */
	public function create_payment_entry(PaymentEntry $entry) {
		$result = $this->post_object("Payment%20Entry", $entry);
		if (property_exists($result, "exc_type")) {
			$messages = [];
			$server_messages = json_decode($result->_server_messages, true);
			foreach ($server_messages as $message) {
				$messages[] = json_decode($message, true)["message"];
			}
			throw new ErpNextException(implode(", ", $messages));
		}
		return new PaymentEntry($result->data);
	}

	/**
	 * Get a specific Payment Entry Reference.
	 * @param string $key The key of the Payment entry.
	 * @return PaymentEntryReference
	 * @throws ErpNextException
	 */
	public function get_payment_entry_reference($key) {
		return $this->get_object("Payment%20Entry%20Reference", $key)->data;
//		return new PaymentEntry($this->get_object("Payment%20Entry", $key)->data);
	}

	/**
	 * Get a list of Payment Entry References.
	 * @param string $fields A string containing the fields to retrieve (e.g. ["name","first_name"])
	 * @param string $filters A string containing the filters (e.g. [["Person","first_name","=","Jane"]])
	 * @return \stdClass
	 * @throws ErpNextException
	 */
	public function get_payment_entry_references($fields = "", $filters = "") {
		return $this->get_object_list("Payment%20Entry%20Reference", $fields, $filters);
	}

	/**
	 * Create a new Payment Entry Reference.
	 * @param PaymentEntryReference $entry The Payment entry object containing the data to create
	 * @return PaymentEntryReference
	 * @throws ErpNextException
	 */
	public function create_payment_entry_reference(PaymentEntryReference $entry) {
		return new PaymentEntryReference($this->post_object("Payment%20Entry%20Reference", $entry)->data);
	}

	/**
	 * Get a specific item.
	 * @param string $key The key of the item.
	 * @return Item
	 * @throws ErpNextException
	 */
	public function get_item($key) {
		return new Item($this->get_object("Item", $key));
	}

	/**
	 * Get a list of items.
	 * @param string $fields A string containing the fields to retrieve (e.g. ["name","first_name"])
	 * @param string $filters A string containing the filters (e.g. [["Person","first_name","=","Jane"]])
	 * @return \stdClass
	 * @throws ErpNextException
	 */
	public function get_items($fields = "", $filters = "") {
		return $this->get_object_list("Item", $fields, $filters);
	}

	/**
	 * Create a new item.
	 * @param Item $item The item object containing the data to create
	 * @return Item
	 * @throws ErpNextException
	 */
	public function create_item(Item $item) {
		return new Item($this->post_object("Item", $item));
	}

	/**
	 * Get a specific ledger.
	 * @param string $key The key of the ledger
	 * @return \stdClass
	 * @throws ErpNextException
	 */
	public function get_ledger($key) {
		return $this->get_object("Account", $key);
	}

	/**
	 * Get a list of ledgers.
	 * @param string $fields A string containing the fields to retrieve (e.g. ["name","first_name"])
	 * @param string $filters A string containing the filters (e.g. [["Person","first_name","=","Jane"]])
	 * @return \stdClass
	 * @throws ErpNextException
	 */
	public function get_ledgers($fields = "", $filters = "") {
		return $this->get_object_list("Account", $fields, $filters);
	}

	/**
	 * Upload an attachment to a certain document.
	 * @param string $doctype The ERP Next doctype to add the attachment to
	 * @param string $docname The name of the document in ERP Next to add the attachment to
	 * @param string $filename The filename of the file to upload
	 * @param string $filedata The file, base64 encoded
	 */
	public function upload_attachment($doctype, $docname, $filename, $filedata) {
		return $this->execute_command("uploadfile", $doctype, $docname, ["filename" => $filename, "filedata" => $filedata, "from_form" => 1, "is_private" => 1]);
	}

	// TODO: Add an add attachment call in the PO class

	/**
	 * Execute a command on ERP Next.
	 * @param string $method The method to execute
	 * @param string $doctype The ERP Next doctype to execute the command against
	 * @param string $docname The name of the document in ERP Next to execute the command against
	 * @param array $data The data to post to the command
	 * @return \stdClass
	 * @throws ErpNextException
	 */
	protected function execute_command($method, $doctype, $docname, $data) {
		$ch = curl_init();

		$payload = $data;
		$payload["doctype"] = $doctype;
		$payload["docname"] = $docname;

		curl_setopt($ch, CURLOPT_URL, 'https://' . $this->instance . '/api/method/' . $method);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);

		if (!$this->verify_ssl) {
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		}

		$headers = array();
		$headers[] = 'Authorization: token ' . $this->auth_user . ':' . $this->auth_token;
		$headers[] = 'Host: ' . $this->host;
		$headers[] = 'Accept: application/json';
		$headers[] = 'Content-Type: multipart/form-data';
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		$result = curl_exec($ch);
		$err = curl_errno($ch);
		curl_close($ch);
		if ($err) {
			throw new ERPNextException("cUrl error #" . $err);
		} else {
			return json_decode($result);
		}
	}



	/**
	 * @param string $doctype The ERP Next doctype to post
	 * @param ERPNextObject $data An ERPNextObject instance for the given doctype
	 * @return \stdClass
	 * @throws ErpNextException
	 */
	protected function post_object($doctype, $data) {
		$ch = curl_init();

		$payload = ['data' => $data->__toString()];
//		curl_setopt($ch, CURLOPT_URL, 'https://boekhouding.euroskano.nl/api/resource/Customer');
		curl_setopt($ch, CURLOPT_URL, 'https://' . $this->instance . '/api/resource/' . $doctype);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);

		if (!$this->verify_ssl) {
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		}

		$headers = array();
		$headers[] = 'Authorization: token ' . $this->auth_user . ':' . $this->auth_token;
		$headers[] = 'Host: ' . $this->host;
		$headers[] = 'Accept: application/json';
		$headers[] = 'Content-Type: multipart/form-data';
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		$result = curl_exec($ch);
		$err = curl_errno($ch);
		curl_close($ch);
		if ($err) {
			throw new ERPNextException("cUrl error #" . $err);
		} else {
			return json_decode($result);
		}
	}

	protected function get_object($doctype, $key) {
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, 'https://' . $this->instance . '/api/resource/' . $doctype . '/' . $key);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		$headers = array();
		$headers[] = 'Authorization: token ' . $this->auth_user . ':' . $this->auth_token;
		$headers[] = 'Host: ' . $this->host;
		$headers[] = 'Accept: application/json';
		$headers[] = 'Content-Type: multipart/form-data';
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		if (!$this->verify_ssl) {
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		}

		$result = curl_exec($ch);
		$err = curl_errno($ch);
		curl_close($ch);
		if ($err) {
			throw new ERPNextException("cUrl error #" . $err);
		} else {
			return json_decode($result);
		}
	}

	protected function get_object_list($doctype, $fields = "", $filters = "") {
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, 'https://' . $this->instance . '/api/resource/' . $doctype .
			"?limit_page_length=500" .
			(strlen($fields) > 0 ? "&fields=" . rawurlencode($fields) : "").
			(strlen($filters) > 0 ? "&filters=" . rawurlencode($filters) : ""));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		$headers = array();
		$headers[] = 'Authorization: token ' . $this->auth_user . ':' . $this->auth_token;
		$headers[] = 'Host: ' . $this->host;
		$headers[] = 'Accept: application/json';
		$headers[] = 'Content-Type: multipart/form-data';
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		if (!$this->verify_ssl) {
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		}

		$result = curl_exec($ch);
		$err = curl_errno($ch);
		curl_close($ch);
		if ($err) {
			throw new ERPNextException("cUrl error #" . $err);
		} else {
			return json_decode($result, JSON_OBJECT_AS_ARRAY);
		}
	}
}
