<?php
/**
 * (c) Jan Thiemen <jan.thiemen@euroskano.nl>
 *
 * This source file is subject to the GNU General Public License (v3) that is bundled
 * with this source code in the file LICENSE.
 */

namespace janthiemen\erpnextphpapi;

class SalesInvoice extends ERPNextObject {

	/** @var $base_write_off_amount string */
	protected $base_write_off_amount;
	/** @var $modified_by string */
	protected $modified_by;
	/** @var $title string */
	protected $title;
	/** @var $selling_price_list string */
	protected $selling_price_list;
	/** @var $set_posting_time int */
	protected $set_posting_time;
	/** @var $discount_amount string */
	protected $discount_amount;
	/** @var $due_date string */
	protected $due_date;
	/** @var $doctype string */
	protected $doctype;
	/** @var $ignore_pricing_rule int */
	protected $ignore_pricing_rule;
	/** @var $base_discount_amount string */
	protected $base_discount_amount;
	/** @var $base_total_taxes_and_charges string */
	protected $base_total_taxes_and_charges;
	/** @var $commission_rate string */
	protected $commission_rate;
	/** @var $name string */
	protected $name;
	/** @var $is_return boolean */
	protected $is_return;
	/** @var $write_off_outstanding_amount_automatically int */
	protected $write_off_outstanding_amount_automatically;
	/** @var $redeem_loyalty_points int */
	protected $redeem_loyalty_points;
	/** @var $base_rounding_adjustment string */
	protected $base_rounding_adjustment;
	/** @var $is_pos boolean */
	protected $is_pos;
	/** @var $exported_to_abacus boolean */
	protected $exported_to_abacus;
	/** @var $against_income_account string */
	protected $against_income_account;
	/** @var $write_off_amount string */
	protected $write_off_amount;
	/** @var $update_billed_amount_in_sales_order int */
	protected $update_billed_amount_in_sales_order;
	/** @var $creation string */
	protected $creation;
	/** @var $party_account_currency string */
	protected $party_account_currency;
	/** @var $net_total string */
	protected $net_total;
	/** @var $items array */
	protected $items = [];
	/** @var $owner string */
	protected $owner;
	/** @var $total_qty string */
	protected $total_qty;
	/** @var $posting_time string */
	protected $posting_time;
	/** @var $scan_barcode string */
	protected $scan_barcode;
	/** @var $price_list_currency string */
	protected $price_list_currency;
	/** @var $loyalty_points int */
	protected $loyalty_points;
	/** @var $enable_lsv boolean */
	protected $enable_lsv;
	/** @var $pos_total_qty string */
	protected $pos_total_qty;
	/** @var $is_opening string */
	protected $is_opening;
	/** @var $total_commission string */
	protected $total_commission;
	/** @var $allocate_advances_automatically int */
	protected $allocate_advances_automatically;
	/** @var $c_form_applicable string */
	protected $c_form_applicable;
	/** @var $base_net_total string */
	protected $base_net_total;
	/** @var $idx int */
	protected $idx;
	/** @var $rounded_total string */
	protected $rounded_total;
	/** @var $shipping_address_name string */
	protected $shipping_address_name;
	/** @var $apply_discount_on string */
	protected $apply_discount_on;
	/** @var $in_words string */
	protected $in_words;
	/** @var $additional_discount_percentage string */
	protected $additional_discount_percentage;
	/** @var $base_paid_amount string */
	protected $base_paid_amount;
	/** @var $conversion_rate string */
	protected $conversion_rate;
	/** @var $total_advance string */
	protected $total_advance;
	/** @var $total string */
	protected $total;
	/** @var $payment_reminder_level int */
	protected $payment_reminder_level;
	/** @var $customer_name string */
	protected $customer_name;
	/** @var $update_stock int */
	protected $update_stock;
	/** @var $base_total string */
	protected $base_total;
	/** @var $territory string */
	protected $territory;
	/** @var $company string */
	protected $company;
	/** @var $base_rounded_total string */
	protected $base_rounded_total;
	/** @var $tax_category string */
	protected $tax_category;
	/** @var $customer string */
	protected $customer;
	/** @var $grand_total string */
	protected $grand_total;
	/** @var $language string */
	protected $language;
	/** @var $modified string */
	protected $modified;
	/** @var $rounding_adjustment string */
	protected $rounding_adjustment;
	/** @var $posting_date string */
	protected $posting_date;
	/** @var $customer_group string */
	protected $customer_group;
	/** @var $base_in_words string */
	protected $base_in_words;
	/** @var $naming_series string */
	protected $naming_series;
	/** @var $is_proposed boolean */
	protected $is_proposed;
	/** @var $currency string */
	protected $currency;
	/** @var $company_address_display string */
	protected $company_address_display;
	/** @var $loyalty_amount string */
	protected $loyalty_amount;
	/** @var $paid_amount string */
	protected $paid_amount;
	/** @var $debit_to string */
	protected $debit_to;
	/** @var $base_change_amount string */
	protected $base_change_amount;
	/** @var $base_grand_total string */
	protected $base_grand_total;
	/** @var $docstatus int */
	protected $docstatus;
	/** @var $status string */
	protected $status;
	/** @var $total_billing_amount string */
	protected $total_billing_amount;
	/** @var $group_same_items int */
	protected $group_same_items;
	/** @var $outstanding_amount string */
	protected $outstanding_amount;
	/** @var $change_amount string */
	protected $change_amount;
	/** @var $total_net_weight string */
	protected $total_net_weight;
	/** @var $remarks string */
	protected $remarks;
	/** @var $plc_conversion_rate string */
	protected $plc_conversion_rate;
	/** @var $total_taxes_and_charges string */
	protected $total_taxes_and_charges;

	/**
	 * Constructor for the ERPNext Sales Invoice model
	 * @param $data array The data array as retrieved from ERP Next
	 */
	public function __construct($data = null) {
		parent::__construct($data);
		if (!is_null($data) && isset($data->items)) {
			$this->items = [];
			foreach ($data->items as $item) {
				$this->items[] = new InvoiceItem($item);
			}
		}
	}

	public function __toString() {
		$data = $this->__toArray();
		$data["items"] = [];
		/** @var InvoiceItem $item */
		foreach ($this->items as $item) {
			$data["items"][] = $item->__toArray();
		}
		return json_encode($data, JSON_NUMERIC_CHECK);
	}

	/**
	 * @return string
	 */
	public function getBaseWriteOffAmount(): ?string {
		return $this->base_write_off_amount;
	}

	/**
	 * @param string $base_write_off_amount
	 */
	public function setBaseWriteOffAmount(string $base_write_off_amount) {
		$this->base_write_off_amount = $base_write_off_amount;
	}

	/**
	 * @return string
	 */
	public function getModifiedBy(): ?string {
		return $this->modified_by;
	}

	/**
	 * @param string $modified_by
	 */
	public function setModifiedBy(string $modified_by) {
		$this->modified_by = $modified_by;
	}

	/**
	 * @return string
	 */
	public function getTitle(): ?string {
		return $this->title;
	}

	/**
	 * @param string $title
	 */
	public function setTitle(string $title) {
		$this->title = $title;
	}

	/**
	 * @return string
	 */
	public function getSellingPriceList(): ?string {
		return $this->selling_price_list;
	}

	/**
	 * @param string $selling_price_list
	 */
	public function setSellingPriceList(string $selling_price_list) {
		$this->selling_price_list = $selling_price_list;
	}

	/**
	 * @return int
	 */
	public function getSetPostingTime(): ?int {
		return $this->set_posting_time;
	}

	/**
	 * @param int $set_posting_time
	 */
	public function setSetPostingTime(int $set_posting_time) {
		$this->set_posting_time = $set_posting_time;
	}

	/**
	 * @return string
	 */
	public function getDiscountAmount(): ?string {
		return $this->discount_amount;
	}

	/**
	 * @param string $discount_amount
	 */
	public function setDiscountAmount(string $discount_amount) {
		$this->discount_amount = $discount_amount;
	}

	/**
	 * @return string
	 */
	public function getDueDate(): ?string {
		return $this->due_date;
	}

	/**
	 * @param string $due_date
	 */
	public function setDueDate(string $due_date) {
		$this->due_date = $due_date;
	}

	/**
	 * @return string
	 */
	public function getDoctype(): ?string {
		return $this->doctype;
	}

	/**
	 * @param string $doctype
	 */
	public function setDoctype(string $doctype) {
		$this->doctype = $doctype;
	}

	/**
	 * @return int
	 */
	public function getIgnorePricingRule(): ?int {
		return $this->ignore_pricing_rule;
	}

	/**
	 * @param int $ignore_pricing_rule
	 */
	public function setIgnorePricingRule(int $ignore_pricing_rule) {
		$this->ignore_pricing_rule = $ignore_pricing_rule;
	}

	/**
	 * @return string
	 */
	public function getBaseDiscountAmount(): ?string {
		return $this->base_discount_amount;
	}

	/**
	 * @param string $base_discount_amount
	 */
	public function setBaseDiscountAmount(string $base_discount_amount) {
		$this->base_discount_amount = $base_discount_amount;
	}

	/**
	 * @return string
	 */
	public function getBaseTotalTaxesAndCharges(): ?string {
		return $this->base_total_taxes_and_charges;
	}

	/**
	 * @param string $base_total_taxes_and_charges
	 */
	public function setBaseTotalTaxesAndCharges(string $base_total_taxes_and_charges) {
		$this->base_total_taxes_and_charges = $base_total_taxes_and_charges;
	}

	/**
	 * @return string
	 */
	public function getCommissionRate(): ?string {
		return $this->commission_rate;
	}

	/**
	 * @param string $commission_rate
	 */
	public function setCommissionRate(string $commission_rate) {
		$this->commission_rate = $commission_rate;
	}

	/**
	 * @return string
	 */
	public function getName(): ?string {
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName(string $name) {
		$this->name = $name;
	}

	/**
	 * @return bool
	 */
	public function isReturn(): bool {
		return $this->is_return;
	}

	/**
	 * @param bool $is_return
	 */
	public function setIsReturn(bool $is_return) {
		$this->is_return = $is_return;
	}

	/**
	 * @return int
	 */
	public function getWriteOffOutstandingAmountAutomatically(): ?int {
		return $this->write_off_outstanding_amount_automatically;
	}

	/**
	 * @param int $write_off_outstanding_amount_automatically
	 */
	public function setWriteOffOutstandingAmountAutomatically(int $write_off_outstanding_amount_automatically) {
		$this->write_off_outstanding_amount_automatically = $write_off_outstanding_amount_automatically;
	}

	/**
	 * @return int
	 */
	public function getRedeemLoyaltyPoints(): ?int {
		return $this->redeem_loyalty_points;
	}

	/**
	 * @param int $redeem_loyalty_points
	 */
	public function setRedeemLoyaltyPoints(int $redeem_loyalty_points) {
		$this->redeem_loyalty_points = $redeem_loyalty_points;
	}

	/**
	 * @return string
	 */
	public function getBaseRoundingAdjustment(): ?string {
		return $this->base_rounding_adjustment;
	}

	/**
	 * @param string $base_rounding_adjustment
	 */
	public function setBaseRoundingAdjustment(string $base_rounding_adjustment) {
		$this->base_rounding_adjustment = $base_rounding_adjustment;
	}

	/**
	 * @return bool
	 */
	public function isPos(): bool {
		return $this->is_pos;
	}

	/**
	 * @param bool $is_pos
	 */
	public function setIsPos(bool $is_pos) {
		$this->is_pos = $is_pos;
	}

	/**
	 * @return bool
	 */
	public function isExportedToAbacus(): bool {
		return $this->exported_to_abacus;
	}

	/**
	 * @param bool $exported_to_abacus
	 */
	public function setExportedToAbacus(bool $exported_to_abacus) {
		$this->exported_to_abacus = $exported_to_abacus;
	}

	/**
	 * @return string
	 */
	public function getAgainstIncomeAccount(): ?string {
		return $this->against_income_account;
	}

	/**
	 * @param string $against_income_account
	 */
	public function setAgainstIncomeAccount(string $against_income_account) {
		$this->against_income_account = $against_income_account;
	}

	/**
	 * @return string
	 */
	public function getWriteOffAmount(): ?string {
		return $this->write_off_amount;
	}

	/**
	 * @param string $write_off_amount
	 */
	public function setWriteOffAmount(string $write_off_amount) {
		$this->write_off_amount = $write_off_amount;
	}

	/**
	 * @return int
	 */
	public function getUpdateBilledAmountInSalesOrder(): ?int {
		return $this->update_billed_amount_in_sales_order;
	}

	/**
	 * @param int $update_billed_amount_in_sales_order
	 */
	public function setUpdateBilledAmountInSalesOrder(int $update_billed_amount_in_sales_order) {
		$this->update_billed_amount_in_sales_order = $update_billed_amount_in_sales_order;
	}

	/**
	 * @return string
	 */
	public function getCreation(): ?string {
		return $this->creation;
	}

	/**
	 * @param string $creation
	 */
	public function setCreation(string $creation) {
		$this->creation = $creation;
	}

	/**
	 * @return string
	 */
	public function getPartyAccountCurrency(): ?string {
		return $this->party_account_currency;
	}

	/**
	 * @param string $party_account_currency
	 */
	public function setPartyAccountCurrency(string $party_account_currency) {
		$this->party_account_currency = $party_account_currency;
	}

	/**
	 * @return string
	 */
	public function getNetTotal(): ?string {
		return $this->net_total;
	}

	/**
	 * @param string $net_total
	 */
	public function setNetTotal(string $net_total) {
		$this->net_total = $net_total;
	}

	/**
	 * @return array
	 */
	public function getItems(): array {
		return $this->items;
	}

	/**
	 * @param array $items
	 */
	public function setItems(array $items) {
		$this->items = $items;
	}

	/**
	 * @param InvoiceItem $item
	 */
	public function addItem(InvoiceItem $item) {
		$this->items[] = $item;
	}

	/**
	 * @return string
	 */
	public function getOwner(): ?string {
		return $this->owner;
	}

	/**
	 * @param string $owner
	 */
	public function setOwner(string $owner) {
		$this->owner = $owner;
	}

	/**
	 * @return string
	 */
	public function getTotalQty(): ?string {
		return $this->total_qty;
	}

	/**
	 * @param string $total_qty
	 */
	public function setTotalQty(string $total_qty) {
		$this->total_qty = $total_qty;
	}

	/**
	 * @return string
	 */
	public function getPostingTime(): ?string {
		return $this->posting_time;
	}

	/**
	 * @param string $posting_time
	 */
	public function setPostingTime(string $posting_time) {
		$this->posting_time = $posting_time;
	}

	/**
	 * @return string
	 */
	public function getScanBarcode(): ?string {
		return $this->scan_barcode;
	}

	/**
	 * @param string $scan_barcode
	 */
	public function setScanBarcode(string $scan_barcode) {
		$this->scan_barcode = $scan_barcode;
	}

	/**
	 * @return string
	 */
	public function getPriceListCurrency(): ?string {
		return $this->price_list_currency;
	}

	/**
	 * @param string $price_list_currency
	 */
	public function setPriceListCurrency(string $price_list_currency) {
		$this->price_list_currency = $price_list_currency;
	}

	/**
	 * @return int
	 */
	public function getLoyaltyPoints(): ?int {
		return $this->loyalty_points;
	}

	/**
	 * @param int $loyalty_points
	 */
	public function setLoyaltyPoints(int $loyalty_points) {
		$this->loyalty_points = $loyalty_points;
	}

	/**
	 * @return bool
	 */
	public function isEnableLsv(): bool {
		return $this->enable_lsv;
	}

	/**
	 * @param bool $enable_lsv
	 */
	public function setEnableLsv(bool $enable_lsv) {
		$this->enable_lsv = $enable_lsv;
	}

	/**
	 * @return string
	 */
	public function getPosTotalQty(): ?string {
		return $this->pos_total_qty;
	}

	/**
	 * @param string $pos_total_qty
	 */
	public function setPosTotalQty(string $pos_total_qty) {
		$this->pos_total_qty = $pos_total_qty;
	}

	/**
	 * @return string
	 */
	public function getisOpening(): ?string {
		return $this->is_opening;
	}

	/**
	 * @param string $is_opening
	 */
	public function setIsOpening(string $is_opening) {
		$this->is_opening = $is_opening;
	}

	/**
	 * @return string
	 */
	public function getTotalCommission(): ?string {
		return $this->total_commission;
	}

	/**
	 * @param string $total_commission
	 */
	public function setTotalCommission(string $total_commission) {
		$this->total_commission = $total_commission;
	}

	/**
	 * @return int
	 */
	public function getAllocateAdvancesAutomatically(): ?int {
		return $this->allocate_advances_automatically;
	}

	/**
	 * @param int $allocate_advances_automatically
	 */
	public function setAllocateAdvancesAutomatically(int $allocate_advances_automatically) {
		$this->allocate_advances_automatically = $allocate_advances_automatically;
	}

	/**
	 * @return string
	 */
	public function getCFormApplicable(): ?string {
		return $this->c_form_applicable;
	}

	/**
	 * @param string $c_form_applicable
	 */
	public function setCFormApplicable(string $c_form_applicable) {
		$this->c_form_applicable = $c_form_applicable;
	}

	/**
	 * @return string
	 */
	public function getBaseNetTotal(): ?string {
		return $this->base_net_total;
	}

	/**
	 * @param string $base_net_total
	 */
	public function setBaseNetTotal(string $base_net_total) {
		$this->base_net_total = $base_net_total;
	}

	/**
	 * @return int
	 */
	public function getIdx(): ?int {
		return $this->idx;
	}

	/**
	 * @param int $idx
	 */
	public function setIdx(int $idx) {
		$this->idx = $idx;
	}

	/**
	 * @return string
	 */
	public function getRoundedTotal(): ?string {
		return $this->rounded_total;
	}

	/**
	 * @param string $rounded_total
	 */
	public function setRoundedTotal(string $rounded_total) {
		$this->rounded_total = $rounded_total;
	}

	/**
	 * @return string
	 */
	public function getShippingAddressName(): ?string {
		return $this->shipping_address_name;
	}

	/**
	 * @param string $shipping_address_name
	 */
	public function setShippingAddressName(string $shipping_address_name) {
		$this->shipping_address_name = $shipping_address_name;
	}

	/**
	 * @return string
	 */
	public function getApplyDiscountOn(): ?string {
		return $this->apply_discount_on;
	}

	/**
	 * @param string $apply_discount_on
	 */
	public function setApplyDiscountOn(string $apply_discount_on) {
		$this->apply_discount_on = $apply_discount_on;
	}

	/**
	 * @return string
	 */
	public function getInWords(): ?string {
		return $this->in_words;
	}

	/**
	 * @param string $in_words
	 */
	public function setInWords(string $in_words) {
		$this->in_words = $in_words;
	}

	/**
	 * @return string
	 */
	public function getAdditionalDiscountPercentage(): ?string {
		return $this->additional_discount_percentage;
	}

	/**
	 * @param string $additional_discount_percentage
	 */
	public function setAdditionalDiscountPercentage(string $additional_discount_percentage) {
		$this->additional_discount_percentage = $additional_discount_percentage;
	}

	/**
	 * @return string
	 */
	public function getBasePaidAmount(): ?string {
		return $this->base_paid_amount;
	}

	/**
	 * @param string $base_paid_amount
	 */
	public function setBasePaidAmount(string $base_paid_amount) {
		$this->base_paid_amount = $base_paid_amount;
	}

	/**
	 * @return string
	 */
	public function getConversionRate(): ?string {
		return $this->conversion_rate;
	}

	/**
	 * @param string $conversion_rate
	 */
	public function setConversionRate(string $conversion_rate) {
		$this->conversion_rate = $conversion_rate;
	}

	/**
	 * @return string
	 */
	public function getTotalAdvance(): ?string {
		return $this->total_advance;
	}

	/**
	 * @param string $total_advance
	 */
	public function setTotalAdvance(string $total_advance) {
		$this->total_advance = $total_advance;
	}

	/**
	 * @return string
	 */
	public function getTotal(): ?string {
		return $this->total;
	}

	/**
	 * @param string $total
	 */
	public function setTotal(string $total) {
		$this->total = $total;
	}

	/**
	 * @return int
	 */
	public function getPaymentReminderLevel(): ?int {
		return $this->payment_reminder_level;
	}

	/**
	 * @param int $payment_reminder_level
	 */
	public function setPaymentReminderLevel(int $payment_reminder_level) {
		$this->payment_reminder_level = $payment_reminder_level;
	}

	/**
	 * @return string
	 */
	public function getCustomerName(): ?string {
		return $this->customer_name;
	}

	/**
	 * @param string $customer_name
	 */
	public function setCustomerName(string $customer_name) {
		$this->customer_name = $customer_name;
	}

	/**
	 * @return int
	 */
	public function getUpdateStock(): ?int {
		return $this->update_stock;
	}

	/**
	 * @param int $update_stock
	 */
	public function setUpdateStock(int $update_stock) {
		$this->update_stock = $update_stock;
	}

	/**
	 * @return string
	 */
	public function getBaseTotal(): ?string {
		return $this->base_total;
	}

	/**
	 * @param string $base_total
	 */
	public function setBaseTotal(string $base_total) {
		$this->base_total = $base_total;
	}

	/**
	 * @return string
	 */
	public function getTerritory(): ?string {
		return $this->territory;
	}

	/**
	 * @param string $territory
	 */
	public function setTerritory(string $territory) {
		$this->territory = $territory;
	}

	/**
	 * @return string
	 */
	public function getCompany(): ?string {
		return $this->company;
	}

	/**
	 * @param string $company
	 */
	public function setCompany(string $company) {
		$this->company = $company;
	}

	/**
	 * @return string
	 */
	public function getBaseRoundedTotal(): ?string {
		return $this->base_rounded_total;
	}

	/**
	 * @param string $base_rounded_total
	 */
	public function setBaseRoundedTotal(string $base_rounded_total) {
		$this->base_rounded_total = $base_rounded_total;
	}

	/**
	 * @return string
	 */
	public function getTaxCategory(): ?string {
		return $this->tax_category;
	}

	/**
	 * @param string $tax_category
	 */
	public function setTaxCategory(string $tax_category) {
		$this->tax_category = $tax_category;
	}

	/**
	 * @return string
	 */
	public function getCustomer(): ?string {
		return $this->customer;
	}

	/**
	 * @param string $customer
	 */
	public function setCustomer(string $customer) {
		$this->customer = $customer;
	}

	/**
	 * @return string
	 */
	public function getGrandTotal(): ?string {
		return $this->grand_total;
	}

	/**
	 * @param string $grand_total
	 */
	public function setGrandTotal(string $grand_total) {
		$this->grand_total = $grand_total;
	}

	/**
	 * @return string
	 */
	public function getLanguage(): ?string {
		return $this->language;
	}

	/**
	 * @param string $language
	 */
	public function setLanguage(string $language) {
		$this->language = $language;
	}

	/**
	 * @return string
	 */
	public function getModified(): ?string {
		return $this->modified;
	}

	/**
	 * @param string $modified
	 */
	public function setModified(string $modified) {
		$this->modified = $modified;
	}

	/**
	 * @return string
	 */
	public function getRoundingAdjustment(): ?string {
		return $this->rounding_adjustment;
	}

	/**
	 * @param string $rounding_adjustment
	 */
	public function setRoundingAdjustment(string $rounding_adjustment) {
		$this->rounding_adjustment = $rounding_adjustment;
	}

	/**
	 * @return string
	 */
	public function getPostingDate(): ?string {
		return $this->posting_date;
	}

	/**
	 * @param string $posting_date
	 */
	public function setPostingDate(string $posting_date) {
		$this->posting_date = $posting_date;
	}

	/**
	 * @return string
	 */
	public function getCustomerGroup(): ?string {
		return $this->customer_group;
	}

	/**
	 * @param string $customer_group
	 */
	public function setCustomerGroup(string $customer_group) {
		$this->customer_group = $customer_group;
	}

	/**
	 * @return string
	 */
	public function getBaseInWords(): ?string {
		return $this->base_in_words;
	}

	/**
	 * @param string $base_in_words
	 */
	public function setBaseInWords(string $base_in_words) {
		$this->base_in_words = $base_in_words;
	}

	/**
	 * @return string
	 */
	public function getNamingSeries(): ?string {
		return $this->naming_series;
	}

	/**
	 * @param string $naming_series
	 */
	public function setNamingSeries(string $naming_series) {
		$this->naming_series = $naming_series;
	}

	/**
	 * @return bool
	 */
	public function isProposed(): bool {
		return $this->is_proposed;
	}

	/**
	 * @param bool $is_proposed
	 */
	public function setIsProposed(bool $is_proposed) {
		$this->is_proposed = $is_proposed;
	}

	/**
	 * @return string
	 */
	public function getCurrency(): ?string {
		return $this->currency;
	}

	/**
	 * @param string $currency
	 */
	public function setCurrency(string $currency) {
		$this->currency = $currency;
	}

	/**
	 * @return string
	 */
	public function getCompanyAddressDisplay(): ?string {
		return $this->company_address_display;
	}

	/**
	 * @param string $company_address_display
	 */
	public function setCompanyAddressDisplay(string $company_address_display) {
		$this->company_address_display = $company_address_display;
	}

	/**
	 * @return string
	 */
	public function getLoyaltyAmount(): ?string {
		return $this->loyalty_amount;
	}

	/**
	 * @param string $loyalty_amount
	 */
	public function setLoyaltyAmount(string $loyalty_amount) {
		$this->loyalty_amount = $loyalty_amount;
	}

	/**
	 * @return string
	 */
	public function getPaidAmount(): ?string {
		return $this->paid_amount;
	}

	/**
	 * @param string $paid_amount
	 */
	public function setPaidAmount(string $paid_amount) {
		$this->paid_amount = $paid_amount;
	}

	/**
	 * @return string
	 */
	public function getDebitTo(): ?string {
		return $this->debit_to;
	}

	/**
	 * @param string $debit_to
	 */
	public function setDebitTo(string $debit_to) {
		$this->debit_to = $debit_to;
	}

	/**
	 * @return string
	 */
	public function getBaseChangeAmount(): ?string {
		return $this->base_change_amount;
	}

	/**
	 * @param string $base_change_amount
	 */
	public function setBaseChangeAmount(string $base_change_amount) {
		$this->base_change_amount = $base_change_amount;
	}

	/**
	 * @return string
	 */
	public function getBaseGrandTotal(): ?string {
		return $this->base_grand_total;
	}

	/**
	 * @param string $base_grand_total
	 */
	public function setBaseGrandTotal(string $base_grand_total) {
		$this->base_grand_total = $base_grand_total;
	}

	/**
	 * @return int
	 */
	public function getDocstatus(): ?int {
		return $this->docstatus;
	}

	/**
	 * @param int $docstatus
	 */
	public function setDocstatus(int $docstatus) {
		$this->docstatus = $docstatus;
	}

	/**
	 * @return string
	 */
	public function getStatus(): ?string {
		return $this->status;
	}

	/**
	 * @param string $status
	 */
	public function setStatus(string $status) {
		$this->status = $status;
	}

	/**
	 * @return string
	 */
	public function getTotalBillingAmount(): ?string {
		return $this->total_billing_amount;
	}

	/**
	 * @param string $total_billing_amount
	 */
	public function setTotalBillingAmount(string $total_billing_amount) {
		$this->total_billing_amount = $total_billing_amount;
	}

	/**
	 * @return int
	 */
	public function getGroupSameItems(): ?int {
		return $this->group_same_items;
	}

	/**
	 * @param int $group_same_items
	 */
	public function setGroupSameItems(int $group_same_items) {
		$this->group_same_items = $group_same_items;
	}

	/**
	 * @return string
	 */
	public function getOutstandingAmount(): ?string {
		return $this->outstanding_amount;
	}

	/**
	 * @param string $outstanding_amount
	 */
	public function setOutstandingAmount(string $outstanding_amount) {
		$this->outstanding_amount = $outstanding_amount;
	}

	/**
	 * @return string
	 */
	public function getChangeAmount(): ?string {
		return $this->change_amount;
	}

	/**
	 * @param string $change_amount
	 */
	public function setChangeAmount(string $change_amount) {
		$this->change_amount = $change_amount;
	}

	/**
	 * @return string
	 */
	public function getTotalNetWeight(): ?string {
		return $this->total_net_weight;
	}

	/**
	 * @param string $total_net_weight
	 */
	public function setTotalNetWeight(string $total_net_weight) {
		$this->total_net_weight = $total_net_weight;
	}

	/**
	 * @return string
	 */
	public function getRemarks(): ?string {
		return $this->remarks;
	}

	/**
	 * @param string $remarks
	 */
	public function setRemarks(string $remarks) {
		$this->remarks = $remarks;
	}

	/**
	 * @return string
	 */
	public function getPlcConversionRate(): ?string {
		return $this->plc_conversion_rate;
	}

	/**
	 * @param string $plc_conversion_rate
	 */
	public function setPlcConversionRate(string $plc_conversion_rate) {
		$this->plc_conversion_rate = $plc_conversion_rate;
	}

	/**
	 * @return string
	 */
	public function getTotalTaxesAndCharges(): ?string {
		return $this->total_taxes_and_charges;
	}

	/**
	 * @param string $total_taxes_and_charges
	 */
	public function setTotalTaxesAndCharges(string $total_taxes_and_charges) {
		$this->total_taxes_and_charges = $total_taxes_and_charges;
	}

}
