<?php
/**
 * (c) Jan Thiemen <jan.thiemen@euroskano.nl>
 *
 * This source file is subject to the GNU General Public License (v3) that is bundled with this source code in the file
 * LICENSE.
 */

namespace janthiemen\erpnextphpapi;

class Ledger extends ERPNextObject {

	/** @var $creation string */
	protected $creation;
	/** @var $doctype string */
	protected $doctype;
	/** @var $lft int */
	protected $lft;
	/** @var $report_type string */
	protected $report_type;
	/** @var $owner string */
	protected $owner;
	/** @var $account_name string */
	protected $account_name;
	/** @var $freeze_account string */
	protected $freeze_account;
	/** @var $modified_by string */
	protected $modified_by;
	/** @var $root_type string */
	protected $root_type;
	/** @var $inter_company_account int */
	protected $inter_company_account;
	/** @var $docstatus int */
	protected $docstatus;
	/** @var $account_type string */
	protected $account_type;
	/** @var $company string */
	protected $company;
	/** @var $balance_must_be string */
	protected $balance_must_be;
	/** @var $is_group int */
	protected $is_group;
	/** @var $tax_rate string */
	protected $tax_rate;
	/** @var $account_currency string */
	protected $account_currency;
	/** @var $include_in_gross int */
	protected $include_in_gross;
	/** @var $parent_account string */
	protected $parent_account;
	/** @var $name string */
	protected $name;
	/** @var $idx int */
	protected $idx;
	/** @var $modified string */
	protected $modified;
	/** @var $rgt int */
	protected $rgt;
	/** @var $account_number string */
	protected $account_number;

	/**
	 * Constructor for the ERPNext Ledger model
	 * @param $data array The data array as retrieved from ERP Next
	 */
	public function __construct($data = null) {
		parent::__construct($data);
	}

	/**
	 * @return string
	 */
	public function getCreation(): ?string {
		return $this->creation;
	}

	/**
	 * @param string $creation
	 */
	public function setCreation(string $creation) {
		$this->creation = $creation;
	}

	/**
	 * @return string
	 */
	public function getDoctype(): ?string {
		return $this->doctype;
	}

	/**
	 * @param string $doctype
	 */
	public function setDoctype(string $doctype) {
		$this->doctype = $doctype;
	}

	/**
	 * @return int
	 */
	public function getLft(): int {
		return $this->lft;
	}

	/**
	 * @param int $lft
	 */
	public function setLft(int $lft) {
		$this->lft = $lft;
	}

	/**
	 * @return string
	 */
	public function getReportType(): ?string {
		return $this->report_type;
	}

	/**
	 * @param string $report_type
	 */
	public function setReportType(string $report_type) {
		$this->report_type = $report_type;
	}

	/**
	 * @return string
	 */
	public function getOwner(): ?string {
		return $this->owner;
	}

	/**
	 * @param string $owner
	 */
	public function setOwner(string $owner) {
		$this->owner = $owner;
	}

	/**
	 * @return string
	 */
	public function getAccountName(): ?string {
		return $this->account_name;
	}

	/**
	 * @param string $account_name
	 */
	public function setAccountName(string $account_name) {
		$this->account_name = $account_name;
	}

	/**
	 * @return string
	 */
	public function getFreezeAccount(): ?string {
		return $this->freeze_account;
	}

	/**
	 * @param string $freeze_account
	 */
	public function setFreezeAccount(string $freeze_account) {
		$this->freeze_account = $freeze_account;
	}

	/**
	 * @return string
	 */
	public function getModifiedBy(): ?string {
		return $this->modified_by;
	}

	/**
	 * @param string $modified_by
	 */
	public function setModifiedBy(string $modified_by) {
		$this->modified_by = $modified_by;
	}

	/**
	 * @return string
	 */
	public function getRootType(): ?string {
		return $this->root_type;
	}

	/**
	 * @param string $root_type
	 */
	public function setRootType(string $root_type) {
		$this->root_type = $root_type;
	}

	/**
	 * @return int
	 */
	public function getInterCompanyAccount(): int {
		return $this->inter_company_account;
	}

	/**
	 * @param int $inter_company_account
	 */
	public function setInterCompanyAccount(int $inter_company_account) {
		$this->inter_company_account = $inter_company_account;
	}

	/**
	 * @return int
	 */
	public function getDocstatus(): int {
		return $this->docstatus;
	}

	/**
	 * @param int $docstatus
	 */
	public function setDocstatus(int $docstatus) {
		$this->docstatus = $docstatus;
	}

	/**
	 * @return string
	 */
	public function getAccountType(): ?string {
		return $this->account_type;
	}

	/**
	 * @param string $account_type
	 */
	public function setAccountType(string $account_type) {
		$this->account_type = $account_type;
	}

	/**
	 * @return string
	 */
	public function getCompany(): ?string {
		return $this->company;
	}

	/**
	 * @param string $company
	 */
	public function setCompany(string $company) {
		$this->company = $company;
	}

	/**
	 * @return string
	 */
	public function getBalanceMustBe(): ?string {
		return $this->balance_must_be;
	}

	/**
	 * @param string $balance_must_be
	 */
	public function setBalanceMustBe(string $balance_must_be) {
		$this->balance_must_be = $balance_must_be;
	}

	/**
	 * @return int
	 */
	public function getisGroup(): int {
		return $this->is_group;
	}

	/**
	 * @param int $is_group
	 */
	public function setIsGroup(int $is_group) {
		$this->is_group = $is_group;
	}

	/**
	 * @return string
	 */
	public function getTaxRate(): ?string {
		return $this->tax_rate;
	}

	/**
	 * @param string $tax_rate
	 */
	public function setTaxRate(string $tax_rate) {
		$this->tax_rate = $tax_rate;
	}

	/**
	 * @return string
	 */
	public function getAccountCurrency(): ?string {
		return $this->account_currency;
	}

	/**
	 * @param string $account_currency
	 */
	public function setAccountCurrency(string $account_currency) {
		$this->account_currency = $account_currency;
	}

	/**
	 * @return int
	 */
	public function getIncludeInGross(): int {
		return $this->include_in_gross;
	}

	/**
	 * @param int $include_in_gross
	 */
	public function setIncludeInGross(int $include_in_gross) {
		$this->include_in_gross = $include_in_gross;
	}

	/**
	 * @return string
	 */
	public function getParentAccount(): ?string {
		return $this->parent_account;
	}

	/**
	 * @param string $parent_account
	 */
	public function setParentAccount(string $parent_account) {
		$this->parent_account = $parent_account;
	}

	/**
	 * @return string
	 */
	public function getName(): ?string {
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName(string $name) {
		$this->name = $name;
	}

	/**
	 * @return int
	 */
	public function getIdx(): int {
		return $this->idx;
	}

	/**
	 * @param int $idx
	 */
	public function setIdx(int $idx) {
		$this->idx = $idx;
	}

	/**
	 * @return string
	 */
	public function getModified(): ?string {
		return $this->modified;
	}

	/**
	 * @param string $modified
	 */
	public function setModified(string $modified) {
		$this->modified = $modified;
	}

	/**
	 * @return int
	 */
	public function getRgt(): int {
		return $this->rgt;
	}

	/**
	 * @param int $rgt
	 */
	public function setRgt(int $rgt) {
		$this->rgt = $rgt;
	}

	/**
	 * @return string
	 */
	public function getAccountNumber(): ?string {
		return $this->account_number;
	}

	/**
	 * @param string $account_number
	 */
	public function setAccountNumber(string $account_number) {
		$this->account_number = $account_number;
	}

}
