<?php
/**
 * (c) Jan Thiemen <jan.thiemen@euroskano.nl>
 *
 * This source file is subject to the GNU General Public License (v3) that is bundled
 * with this source code in the file LICENSE.
 */

namespace Janthiemen\Erpnextphpapi;

class PaymentEntryReference extends ERPNextObject {

	/** @var $reference_doctype string */
	protected $reference_doctype;
	/** @var $total_amount string */
	protected $total_amount;
	/** @var $modified_by string */
	protected $modified_by;
	/** @var $name string */
	protected $name;
	/** @var $parent string */
	protected $parent;
	/** @var $due_date string */
	protected $due_date;
	/** @var $reference_name string */
	protected $reference_name;
	/** @var $creation string */
	protected $creation;
	/** @var $modified string */
	protected $modified;
	/** @var $outstanding_amount string */
	protected $outstanding_amount;
	/** @var $idx int */
	protected $idx;
	/** @var $parenttype string */
	protected $parenttype;
	/** @var $doctype string */
	protected $doctype;
	/** @var $owner string */
	protected $owner;
	/** @var $docstatus int */
	protected $docstatus;
	/** @var $allocated_amount string */
	protected $allocated_amount;
	/** @var $exchange_rate string */
	protected $exchange_rate;
	/** @var $parentfield string */
	protected $parentfield;

	/**
	 * Constructor for the ERPNext Item model
	 * @param $data array The data array as retrieved from ERP Next
	 */
	public function __construct($data = null) {
		parent::__construct($data);
	}

	/**
	 * @return string
	 */
	public function getReferenceDoctype(): ?string {
		return $this->reference_doctype;
	}

	/**
	 * @param string $reference_doctype
	 */
	public function setReferenceDoctype(string $reference_doctype) {
		$this->reference_doctype = $reference_doctype;
	}

	/**
	 * @return string
	 */
	public function getTotalAmount(): ?string {
		return $this->total_amount;
	}

	/**
	 * @param string $total_amount
	 */
	public function setTotalAmount(string $total_amount) {
		$this->total_amount = $total_amount;
	}

	/**
	 * @return string
	 */
	public function getModifiedBy(): ?string {
		return $this->modified_by;
	}

	/**
	 * @param string $modified_by
	 */
	public function setModifiedBy(string $modified_by) {
		$this->modified_by = $modified_by;
	}

	/**
	 * @return string
	 */
	public function getName(): ?string {
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName(string $name) {
		$this->name = $name;
	}

	/**
	 * @return string
	 */
	public function getParent(): ?string {
		return $this->parent;
	}

	/**
	 * @param string $parent
	 */
	public function setParent(string $parent) {
		$this->parent = $parent;
	}

	/**
	 * @return string
	 */
	public function getDueDate(): ?string {
		return $this->due_date;
	}

	/**
	 * @param string $due_date
	 */
	public function setDueDate(string $due_date) {
		$this->due_date = $due_date;
	}

	/**
	 * @return string
	 */
	public function getReferenceName(): ?string {
		return $this->reference_name;
	}

	/**
	 * @param string $reference_name
	 */
	public function setReferenceName(string $reference_name) {
		$this->reference_name = $reference_name;
	}

	/**
	 * @return string
	 */
	public function getCreation(): ?string {
		return $this->creation;
	}

	/**
	 * @param string $creation
	 */
	public function setCreation(string $creation) {
		$this->creation = $creation;
	}

	/**
	 * @return string
	 */
	public function getModified(): ?string {
		return $this->modified;
	}

	/**
	 * @param string $modified
	 */
	public function setModified(string $modified) {
		$this->modified = $modified;
	}

	/**
	 * @return string
	 */
	public function getOutstandingAmount(): ?string {
		return $this->outstanding_amount;
	}

	/**
	 * @param string $outstanding_amount
	 */
	public function setOutstandingAmount(string $outstanding_amount) {
		$this->outstanding_amount = $outstanding_amount;
	}

	/**
	 * @return int
	 */
	public function getIdx(): ?int {
		return $this->idx;
	}

	/**
	 * @param int $idx
	 */
	public function setIdx(int $idx) {
		$this->idx = $idx;
	}

	/**
	 * @return string
	 */
	public function getParenttype(): ?string {
		return $this->parenttype;
	}

	/**
	 * @param string $parenttype
	 */
	public function setParenttype(string $parenttype) {
		$this->parenttype = $parenttype;
	}

	/**
	 * @return string
	 */
	public function getDoctype(): ?string {
		return $this->doctype;
	}

	/**
	 * @param string $doctype
	 */
	public function setDoctype(string $doctype) {
		$this->doctype = $doctype;
	}

	/**
	 * @return string
	 */
	public function getOwner(): ?string {
		return $this->owner;
	}

	/**
	 * @param string $owner
	 */
	public function setOwner(string $owner) {
		$this->owner = $owner;
	}

	/**
	 * @return int
	 */
	public function getDocstatus(): ?int {
		return $this->docstatus;
	}

	/**
	 * @param int $docstatus
	 */
	public function setDocstatus(int $docstatus) {
		$this->docstatus = $docstatus;
	}

	/**
	 * @return string
	 */
	public function getAllocatedAmount(): ?string {
		return $this->allocated_amount;
	}

	/**
	 * @param string $allocated_amount
	 */
	public function setAllocatedAmount(string $allocated_amount) {
		$this->allocated_amount = $allocated_amount;
	}

	/**
	 * @return string
	 */
	public function getExchangeRate(): ?string {
		return $this->exchange_rate;
	}

	/**
	 * @param string $exchange_rate
	 */
	public function setExchangeRate(string $exchange_rate) {
		$this->exchange_rate = $exchange_rate;
	}

	/**
	 * @return string
	 */
	public function getParentfield(): ?string {
		return $this->parentfield;
	}

	/**
	 * @param string $parentfield
	 */
	public function setParentfield(string $parentfield) {
		$this->parentfield = $parentfield;
	}
}
