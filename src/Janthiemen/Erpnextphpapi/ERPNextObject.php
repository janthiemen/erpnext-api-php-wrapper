<?php
/**
 * (c) Jan Thiemen <jan.thiemen@euroskano.nl>
 *
 * This source file is subject to the GNU General Public License (v3) that is bundled
 * with this source code in the file LICENSE.
 */

namespace janthiemen\erpnextphpapi;

class ERPNextObject {

	/**
	 * Constructor for the ERPNext base object model
	 * @param $data array The data array as retrieved from ERP Next
	 */
	public function __construct($data = null) {
		if (!is_null($data)) {
			foreach ($data as $key => $item) {
				if (property_exists($this, $key) && !is_object($item) && !is_array($item)) {
					$this->{$key} = $item;
				}
			}
		}
	}

	public function __toArray() {
		$data = [];
		foreach ($this as $key => $value) {
			if (isset($value) && !is_array($value) && !is_object($value)) {
				$data[$key] = $value;
			}
		}
		return $data;
	}

	public function __toString() {
		$data = $this->__toArray();
		return json_encode($data);
	}
}
