<?php
/**
 * (c) Jan Thiemen <jan.thiemen@euroskano.nl>
 *
 * This source file is subject to the GNU General Public License (v3) that is bundled
 * with this source code in the file LICENSE.
 */

namespace Janthiemen\Erpnextphpapi;

class Customer extends ERPNextObject {

	/** @var $naming_series string */
	protected $naming_series;
	/** @var $creation string ISO datetime string */
	protected $creation;
	/** @var $disabled boolean */
	protected $disabled;
	/** @var $owner string */
	protected $owner;
	/** @var $bypass_credit_limit_check_at_sales_order boolean */
	protected $bypass_credit_limit_check_at_sales_order;
	/** @var $customer_name string */
	protected $customer_name;
	/** @var $modified_by string */
	protected $modified_by;
	/** @var $customer_type string */
	protected $customer_type;
	/** @var $default_commission_rate int */
	protected $default_commission_rate;
	/** @var $docstatus int */
	protected $docstatus;
	/** @var $territory string */
	protected $territory;
	/** @var $enable_lsv boolean */
	protected $enable_lsv;
	/** @var $is_internal_customer boolean */
	protected $is_internal_customer;
	/** @var $language string iso2 language code */
	protected $language;
	/** @var $credit_limit int */
	protected $credit_limit;
	/** @var $name string */
	protected $name;
	/** @var $idx int */
	protected $idx;
	/** @var $customer_group string */
	protected $customer_group;
	/** @var $modified string ISO datetime string */
	protected $modified;
	/** @var $is_frozen boolean */
	protected $is_frozen;

	/**
	 * Constructor for the ERPNext Customer model
	 * @param $data array The data array as retrieved from ERP Next
	 */
	public function __construct($data = null) {
		parent::__construct($data);
	}

	/**
	 * @return string
	 */
	public function getNamingSeries(): ?string {
		return $this->naming_series;
	}

	/**
	 * @param string $naming_series
	 */
	public function setNamingSeries(string $naming_series) {
		$this->naming_series = $naming_series;
	}

	/**
	 * @return string
	 */
	public function getCreation() {
		return $this->creation;
	}

	/**
	 * @param string $creation
	 */
	public function setCreation($creation) {
		$this->creation = $creation;
	}

	/**
	 * @return bool
	 */
	public function isDisabled(): bool {
		return $this->disabled;
	}

	/**
	 * @param bool $disabled
	 */
	public function setDisabled(bool $disabled) {
		$this->disabled = $disabled;
	}

	/**
	 * @return string
	 */
	public function getOwner(): ?string {
		return $this->owner;
	}

	/**
	 * @param string $owner
	 */
	public function setOwner(string $owner) {
		$this->owner = $owner;
	}

	/**
	 * @return bool
	 */
	public function isBypassCreditLimitCheckAtSalesOrder(): bool {
		return $this->bypass_credit_limit_check_at_sales_order;
	}

	/**
	 * @param bool $bypass_credit_limit_check_at_sales_order
	 */
	public function setBypassCreditLimitCheckAtSalesOrder(bool $bypass_credit_limit_check_at_sales_order) {
		$this->bypass_credit_limit_check_at_sales_order = $bypass_credit_limit_check_at_sales_order;
	}

	/**
	 * @return string
	 */
	public function getCustomerName(): ?string {
		return $this->customer_name;
	}

	/**
	 * @param string $customer_name
	 */
	public function setCustomerName(string $customer_name) {
		$this->customer_name = $customer_name;
	}

	/**
	 * @return string
	 */
	public function getModifiedBy(): ?string {
		return $this->modified_by;
	}

	/**
	 * @param string $modified_by
	 */
	public function setModifiedBy(string $modified_by) {
		$this->modified_by = $modified_by;
	}

	/**
	 * @return string
	 */
	public function getCustomerType(): ?string {
		return $this->customer_type;
	}

	/**
	 * @param string $customer_type
	 */
	public function setCustomerType(string $customer_type) {
		$this->customer_type = $customer_type;
	}

	/**
	 * @return int
	 */
	public function getDefaultCommissionRate(): int {
		return $this->default_commission_rate;
	}

	/**
	 * @param int $default_commission_rate
	 */
	public function setDefaultCommissionRate(int $default_commission_rate) {
		$this->default_commission_rate = $default_commission_rate;
	}

	/**
	 * @return int
	 */
	public function getDocstatus(): int {
		return $this->docstatus;
	}

	/**
	 * @param int $docstatus
	 */
	public function setDocstatus(int $docstatus) {
		$this->docstatus = $docstatus;
	}

	/**
	 * @return string
	 */
	public function getTerritory(): ?string {
		return $this->territory;
	}

	/**
	 * @param string $territory
	 */
	public function setTerritory(string $territory) {
		$this->territory = $territory;
	}

	/**
	 * @return bool
	 */
	public function isEnableLsv(): bool {
		return $this->enable_lsv;
	}

	/**
	 * @param bool $enable_lsv
	 */
	public function setEnableLsv(bool $enable_lsv) {
		$this->enable_lsv = $enable_lsv;
	}

	/**
	 * @return bool
	 */
	public function isInternalCustomer(): bool {
		return $this->is_internal_customer;
	}

	/**
	 * @param bool $is_internal_customer
	 */
	public function setIsInternalCustomer(bool $is_internal_customer) {
		$this->is_internal_customer = $is_internal_customer;
	}

	/**
	 * @return string
	 */
	public function getLanguage(): ?string {
		return $this->language;
	}

	/**
	 * @param string $language
	 */
	public function setLanguage(string $language) {
		$this->language = $language;
	}

	/**
	 * @return int
	 */
	public function getCreditLimit(): int {
		return $this->credit_limit;
	}

	/**
	 * @param int $credit_limit
	 */
	public function setCreditLimit(int $credit_limit) {
		$this->credit_limit = $credit_limit;
	}

	/**
	 * @return string
	 */
	public function getName(): ?string {
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName(string $name) {
		$this->name = $name;
	}

	/**
	 * @return int
	 */
	public function getIdx(): int {
		return $this->idx;
	}

	/**
	 * @param int $idx
	 */
	public function setIdx(int $idx) {
		$this->idx = $idx;
	}

	/**
	 * @return string
	 */
	public function getCustomerGroup(): ?string {
		return $this->customer_group;
	}

	/**
	 * @param string $customer_group
	 */
	public function setCustomerGroup(string $customer_group) {
		$this->customer_group = $customer_group;
	}

	/**
	 * @return string
	 */
	public function getModified() {
		return $this->modified;
	}

	/**
	 * @param string $modified
	 */
	public function setModified($modified) {
		$this->modified = $modified;
	}

	/**
	 * @return bool
	 */
	public function isFrozen(): bool {
		return $this->is_frozen;
	}

	/**
	 * @param bool $is_frozen
	 */
	public function setIsFrozen(bool $is_frozen) {
		$this->is_frozen = $is_frozen;
	}

}
