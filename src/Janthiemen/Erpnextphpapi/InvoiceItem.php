<?php
/**
 * (c) Jan Thiemen <jan.thiemen@euroskano.nl>
 *
 * This source file is subject to the GNU General Public License (v3) that is bundled
 * with this source code in the file LICENSE.
 */

namespace Janthiemen\Erpnextphpapi;

class InvoiceItem extends ERPNextObject {

	/** @var $item_group string */
	protected $item_group;
	/** @var $stock_qty string */
	protected $stock_qty;
	/** @var $base_price_list_rate string */
	protected $base_price_list_rate;
	/** @var $image string */
	protected $image;
	/** @var $creation string */
	protected $creation;
	/** @var $base_amount string */
	protected $base_amount;
	/** @var $qty string */
	protected $qty;
	/** @var $margin_rate_or_amount string */
	protected $margin_rate_or_amount;
	/** @var $rate string */
	protected $rate;
	/** @var $total_weight string */
	protected $total_weight;
	/** @var $owner string */
	protected $owner;
	/** @var $cost_center string */
	protected $cost_center;
	/** @var $stock_uom string */
	protected $stock_uom;
	/** @var $base_net_amount string */
	protected $base_net_amount;
	/** @var $modified_by string */
	protected $modified_by;
	/** @var $base_net_rate string */
	protected $base_net_rate;
	/** @var $discount_percentage string */
	protected $discount_percentage;
	/** @var $item_name string */
	protected $item_name;
	/** @var $enable_deferred_revenue int */
	protected $enable_deferred_revenue;
	/** @var $amount string */
	protected $amount;
	/** @var $actual_qty string */
	protected $actual_qty;
	/** @var $net_rate string */
	protected $net_rate;
	/** @var $conversion_factor string */
	protected $conversion_factor;
	/** @var $base_rate_with_margin string */
	protected $base_rate_with_margin;
	/** @var $docstatus int */
	protected $docstatus;
	/** @var $actual_batch_qty string */
	protected $actual_batch_qty;
	/** @var $uom string */
	protected $uom;
	/** @var $description string */
	protected $description;
	/** @var $parent string */
	protected $parent;
	/** @var $base_rate string */
	protected $base_rate;
	/** @var $item_code string */
	protected $item_code;
	/** @var $page_break int */
	protected $page_break;
	/** @var $doctype string */
	protected $doctype;
	/** @var $rate_with_margin string */
	protected $rate_with_margin;
	/** @var $expense_account string */
	protected $expense_account;
	/** @var $delivered_qty string */
	protected $delivered_qty;
	/** @var $delivered_by_supplier int */
	protected $delivered_by_supplier;
	/** @var $discount_amount string */
	protected $discount_amount;
	/** @var $price_list_rate string */
	protected $price_list_rate;
	/** @var $allow_zero_valuation_rate int */
	protected $allow_zero_valuation_rate;
	/** @var $name string */
	protected $name;
	/** @var $idx int */
	protected $idx;
	/** @var $item_tax_rate string */
	protected $item_tax_rate;
	/** @var $income_account string */
	protected $income_account;
	/** @var $modified string */
	protected $modified;
	/** @var $weight_per_unit string */
	protected $weight_per_unit;
	/** @var $parenttype string */
	protected $parenttype;
	/** @var $net_amount string */
	protected $net_amount;
	/** @var $is_fixed_asset bool */
	protected $is_fixed_asset;
	/** @var $is_free_item bool */
	protected $is_free_item;
	/** @var $parentfield string */
	protected $parentfield;
	/**
	 * Constructor for the ERPNext Invoice Item model
	 * @param $data array The data array as retrieved from ERP Next
	 */
	public function __construct($data = null) {
		parent::__construct($data);
	}

	/**
	 * @return string
	 */
	public function getItemGroup(): ?string {
		return $this->item_group;
	}

	/**
	 * @param string $item_group
	 */
	public function setItemGroup(string $item_group) {
		$this->item_group = $item_group;
	}

	/**
	 * @return string
	 */
	public function getStockQty(): ?string {
		return $this->stock_qty;
	}

	/**
	 * @param string $stock_qty
	 */
	public function setStockQty(string $stock_qty) {
		$this->stock_qty = $stock_qty;
	}

	/**
	 * @return string
	 */
	public function getBasePriceListRate(): ?string {
		return $this->base_price_list_rate;
	}

	/**
	 * @param string $base_price_list_rate
	 */
	public function setBasePriceListRate(string $base_price_list_rate) {
		$this->base_price_list_rate = $base_price_list_rate;
	}

	/**
	 * @return string
	 */
	public function getImage(): ?string {
		return $this->image;
	}

	/**
	 * @param string $image
	 */
	public function setImage(string $image) {
		$this->image = $image;
	}

	/**
	 * @return string
	 */
	public function getCreation(): ?string {
		return $this->creation;
	}

	/**
	 * @param string $creation
	 */
	public function setCreation(string $creation) {
		$this->creation = $creation;
	}

	/**
	 * @return string
	 */
	public function getBaseAmount(): ?string {
		return $this->base_amount;
	}

	/**
	 * @param string $base_amount
	 */
	public function setBaseAmount(string $base_amount) {
		$this->base_amount = $base_amount;
	}

	/**
	 * @return string
	 */
	public function getQty(): ?string {
		return $this->qty;
	}

	/**
	 * @param string $qty
	 */
	public function setQty(string $qty) {
		$this->qty = $qty;
	}

	/**
	 * @return string
	 */
	public function getMarginRateOrAmount(): ?string {
		return $this->margin_rate_or_amount;
	}

	/**
	 * @param string $margin_rate_or_amount
	 */
	public function setMarginRateOrAmount(string $margin_rate_or_amount) {
		$this->margin_rate_or_amount = $margin_rate_or_amount;
	}

	/**
	 * @return string
	 */
	public function getRate(): ?string {
		return $this->rate;
	}

	/**
	 * @param string $rate
	 */
	public function setRate(string $rate) {
		$this->rate = $rate;
	}

	/**
	 * @return string
	 */
	public function getTotalWeight(): ?string {
		return $this->total_weight;
	}

	/**
	 * @param string $total_weight
	 */
	public function setTotalWeight(string $total_weight) {
		$this->total_weight = $total_weight;
	}

	/**
	 * @return string
	 */
	public function getOwner(): ?string {
		return $this->owner;
	}

	/**
	 * @param string $owner
	 */
	public function setOwner(string $owner) {
		$this->owner = $owner;
	}

	/**
	 * @return string
	 */
	public function getCostCenter(): ?string {
		return $this->cost_center;
	}

	/**
	 * @param string $cost_center
	 */
	public function setCostCenter(string $cost_center) {
		$this->cost_center = $cost_center;
	}

	/**
	 * @return string
	 */
	public function getStockUom(): ?string {
		return $this->stock_uom;
	}

	/**
	 * @param string $stock_uom
	 */
	public function setStockUom(string $stock_uom) {
		$this->stock_uom = $stock_uom;
	}

	/**
	 * @return string
	 */
	public function getBaseNetAmount(): ?string {
		return $this->base_net_amount;
	}

	/**
	 * @param string $base_net_amount
	 */
	public function setBaseNetAmount(string $base_net_amount) {
		$this->base_net_amount = $base_net_amount;
	}

	/**
	 * @return string
	 */
	public function getModifiedBy(): ?string {
		return $this->modified_by;
	}

	/**
	 * @param string $modified_by
	 */
	public function setModifiedBy(string $modified_by) {
		$this->modified_by = $modified_by;
	}

	/**
	 * @return string
	 */
	public function getBaseNetRate(): ?string {
		return $this->base_net_rate;
	}

	/**
	 * @param string $base_net_rate
	 */
	public function setBaseNetRate(string $base_net_rate) {
		$this->base_net_rate = $base_net_rate;
	}

	/**
	 * @return string
	 */
	public function getDiscountPercentage(): ?string {
		return $this->discount_percentage;
	}

	/**
	 * @param string $discount_percentage
	 */
	public function setDiscountPercentage(string $discount_percentage) {
		$this->discount_percentage = $discount_percentage;
	}

	/**
	 * @return string
	 */
	public function getItemName(): ?string {
		return $this->item_name;
	}

	/**
	 * @param string $item_name
	 */
	public function setItemName(string $item_name) {
		$this->item_name = $item_name;
	}

	/**
	 * @return int
	 */
	public function getEnableDeferredRevenue(): int {
		return $this->enable_deferred_revenue;
	}

	/**
	 * @param int $enable_deferred_revenue
	 */
	public function setEnableDeferredRevenue(int $enable_deferred_revenue) {
		$this->enable_deferred_revenue = $enable_deferred_revenue;
	}

	/**
	 * @return string
	 */
	public function getAmount(): ?string {
		return $this->amount;
	}

	/**
	 * @param string $amount
	 */
	public function setAmount(string $amount) {
		$this->amount = $amount;
	}

	/**
	 * @return string
	 */
	public function getActualQty(): ?string {
		return $this->actual_qty;
	}

	/**
	 * @param string $actual_qty
	 */
	public function setActualQty(string $actual_qty) {
		$this->actual_qty = $actual_qty;
	}

	/**
	 * @return string
	 */
	public function getNetRate(): ?string {
		return $this->net_rate;
	}

	/**
	 * @param string $net_rate
	 */
	public function setNetRate(string $net_rate) {
		$this->net_rate = $net_rate;
	}

	/**
	 * @return string
	 */
	public function getConversionFactor(): ?string {
		return $this->conversion_factor;
	}

	/**
	 * @param string $conversion_factor
	 */
	public function setConversionFactor(string $conversion_factor) {
		$this->conversion_factor = $conversion_factor;
	}

	/**
	 * @return string
	 */
	public function getBaseRateWithMargin(): ?string {
		return $this->base_rate_with_margin;
	}

	/**
	 * @param string $base_rate_with_margin
	 */
	public function setBaseRateWithMargin(string $base_rate_with_margin) {
		$this->base_rate_with_margin = $base_rate_with_margin;
	}

	/**
	 * @return int
	 */
	public function getDocstatus(): int {
		return $this->docstatus;
	}

	/**
	 * @param int $docstatus
	 */
	public function setDocstatus(int $docstatus) {
		$this->docstatus = $docstatus;
	}

	/**
	 * @return string
	 */
	public function getActualBatchQty(): ?string {
		return $this->actual_batch_qty;
	}

	/**
	 * @param string $actual_batch_qty
	 */
	public function setActualBatchQty(string $actual_batch_qty) {
		$this->actual_batch_qty = $actual_batch_qty;
	}

	/**
	 * @return string
	 */
	public function getUom(): ?string {
		return $this->uom;
	}

	/**
	 * @param string $uom
	 */
	public function setUom(string $uom) {
		$this->uom = $uom;
	}

	/**
	 * @return string
	 */
	public function getDescription(): ?string {
		return $this->description;
	}

	/**
	 * @param string $description
	 */
	public function setDescription(string $description) {
		$this->description = $description;
	}

	/**
	 * @return string
	 */
	public function getParent(): ?string {
		return $this->parent;
	}

	/**
	 * @param string $parent
	 */
	public function setParent(string $parent) {
		$this->parent = $parent;
	}

	/**
	 * @return string
	 */
	public function getBaseRate(): ?string {
		return $this->base_rate;
	}

	/**
	 * @param string $base_rate
	 */
	public function setBaseRate(string $base_rate) {
		$this->base_rate = $base_rate;
	}

	/**
	 * @return string
	 */
	public function getItemCode(): ?string {
		return $this->item_code;
	}

	/**
	 * @param string $item_code
	 */
	public function setItemCode(string $item_code) {
		$this->item_code = $item_code;
	}

	/**
	 * @return int
	 */
	public function getPageBreak(): int {
		return $this->page_break;
	}

	/**
	 * @param int $page_break
	 */
	public function setPageBreak(int $page_break) {
		$this->page_break = $page_break;
	}

	/**
	 * @return string
	 */
	public function getDoctype(): ?string {
		return $this->doctype;
	}

	/**
	 * @param string $doctype
	 */
	public function setDoctype(string $doctype) {
		$this->doctype = $doctype;
	}

	/**
	 * @return string
	 */
	public function getRateWithMargin(): ?string {
		return $this->rate_with_margin;
	}

	/**
	 * @param string $rate_with_margin
	 */
	public function setRateWithMargin(string $rate_with_margin) {
		$this->rate_with_margin = $rate_with_margin;
	}

	/**
	 * @return string
	 */
	public function getExpenseAccount(): ?string {
		return $this->expense_account;
	}

	/**
	 * @param string $expense_account
	 */
	public function setExpenseAccount(string $expense_account) {
		$this->expense_account = $expense_account;
	}

	/**
	 * @return string
	 */
	public function getDeliveredQty(): ?string {
		return $this->delivered_qty;
	}

	/**
	 * @param string $delivered_qty
	 */
	public function setDeliveredQty(string $delivered_qty) {
		$this->delivered_qty = $delivered_qty;
	}

	/**
	 * @return int
	 */
	public function getDeliveredBySupplier(): int {
		return $this->delivered_by_supplier;
	}

	/**
	 * @param int $delivered_by_supplier
	 */
	public function setDeliveredBySupplier(int $delivered_by_supplier) {
		$this->delivered_by_supplier = $delivered_by_supplier;
	}

	/**
	 * @return string
	 */
	public function getDiscountAmount(): ?string {
		return $this->discount_amount;
	}

	/**
	 * @param string $discount_amount
	 */
	public function setDiscountAmount(string $discount_amount) {
		$this->discount_amount = $discount_amount;
	}

	/**
	 * @return string
	 */
	public function getPriceListRate(): ?string {
		return $this->price_list_rate;
	}

	/**
	 * @param string $price_list_rate
	 */
	public function setPriceListRate(string $price_list_rate) {
		$this->price_list_rate = $price_list_rate;
	}

	/**
	 * @return int
	 */
	public function getAllowZeroValuationRate(): int {
		return $this->allow_zero_valuation_rate;
	}

	/**
	 * @param int $allow_zero_valuation_rate
	 */
	public function setAllowZeroValuationRate(int $allow_zero_valuation_rate) {
		$this->allow_zero_valuation_rate = $allow_zero_valuation_rate;
	}

	/**
	 * @return string
	 */
	public function getName(): ?string {
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName(string $name) {
		$this->name = $name;
	}

	/**
	 * @return int
	 */
	public function getIdx(): int {
		return $this->idx;
	}

	/**
	 * @param int $idx
	 */
	public function setIdx(int $idx) {
		$this->idx = $idx;
	}

	/**
	 * @return string
	 */
	public function getItemTaxRate(): ?string {
		return $this->item_tax_rate;
	}

	/**
	 * @param string $item_tax_rate
	 */
	public function setItemTaxRate(string $item_tax_rate) {
		$this->item_tax_rate = $item_tax_rate;
	}

	/**
	 * @return string
	 */
	public function getIncomeAccount(): ?string {
		return $this->income_account;
	}

	/**
	 * @param string $income_account
	 */
	public function setIncomeAccount(string $income_account) {
		$this->income_account = $income_account;
	}

	/**
	 * @return string
	 */
	public function getModified(): ?string {
		return $this->modified;
	}

	/**
	 * @param string $modified
	 */
	public function setModified(string $modified) {
		$this->modified = $modified;
	}

	/**
	 * @return string
	 */
	public function getWeightPerUnit(): ?string {
		return $this->weight_per_unit;
	}

	/**
	 * @param string $weight_per_unit
	 */
	public function setWeightPerUnit(string $weight_per_unit) {
		$this->weight_per_unit = $weight_per_unit;
	}

	/**
	 * @return string
	 */
	public function getParenttype(): ?string {
		return $this->parenttype;
	}

	/**
	 * @param string $parenttype
	 */
	public function setParenttype(string $parenttype) {
		$this->parenttype = $parenttype;
	}

	/**
	 * @return string
	 */
	public function getNetAmount(): ?string {
		return $this->net_amount;
	}

	/**
	 * @param string $net_amount
	 */
	public function setNetAmount(string $net_amount) {
		$this->net_amount = $net_amount;
	}

	/**
	 * @return bool
	 */
	public function isFixedAsset(): bool {
		return $this->is_fixed_asset;
	}

	/**
	 * @param bool $is_fixed_asset
	 */
	public function setIsFixedAsset(bool $is_fixed_asset) {
		$this->is_fixed_asset = $is_fixed_asset;
	}

	/**
	 * @return bool
	 */
	public function isFreeItem(): bool {
		return $this->is_free_item;
	}

	/**
	 * @param bool $is_free_item
	 */
	public function setIsFreeItem(bool $is_free_item) {
		$this->is_free_item = $is_free_item;
	}

	/**
	 * @return string
	 */
	public function getParentfield(): ?string {
		return $this->parentfield;
	}

	/**
	 * @param string $parentfield
	 */
	public function setParentfield(string $parentfield) {
		$this->parentfield = $parentfield;
	}

}
