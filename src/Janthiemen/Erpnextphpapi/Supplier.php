<?php
/**
 * (c) Jan Thiemen <jan.thiemen@euroskano.nl>
 *
 * This source file is subject to the GNU General Public License (v3) that is bundled
 * with this source code in the file LICENSE.
 */

namespace janthiemen\erpnextphpapi;

class Supplier extends ERPNextObject {

	/** @var $prevent_pos boolean */
	protected $prevent_pos;
	/** @var $is_transporter boolean */
	protected $is_transporter;
	/** @var $creation string ISO datetime string */
	protected $creation;
	/** @var $doctype string */
	protected $doctype;
	/** @var $disabled boolean */
	protected $disabled;
	/** @var $accounts string */
	protected $accounts;
	/** @var $warn_pos boolean */
	protected $warn_pos;
	/** @var $owner string */
	protected $owner;
	/** @var $hold_type string */
	protected $hold_type;
	/** @var $on_hold boolean */
	protected $on_hold;
	/** @var $is_internal_supplier boolean */
	protected $is_internal_supplier;
	/** @var $supplier_group string */
	protected $supplier_group;
	/** @var $modified_by string */
	protected $modified_by;
	/** @var $warn_rfqs boolean */
	protected $warn_rfqs;
	/** @var $naming_series string */
	protected $naming_series;
	/** @var $docstatus boolean */
	protected $docstatus;
	/** @var $language string */
	protected $language;
	/** @var $supplier_name string */
	protected $supplier_name;
	/** @var $supplier_type string */
	protected $supplier_type;
	/** @var $name string */
	protected $name;
	/** @var $idx int */
	protected $idx;
	/** @var $prevent_rfqs boolean */
	protected $prevent_rfqs;
	/** @var $country string */
	protected $country;
	/** @var $modified string ISO datetime string */
	protected $modified;
	/** @var $is_frozen boolean */
	protected $is_frozen;

	/**
	 * Constructor for the ERPNext Customer model
	 * @param $data array The data array as retrieved from ERP Next
	 */
	public function __construct($data = null) {
		parent::__construct($data);
	}

	/**
	 * @return bool
	 */
	public function isPreventPos(): bool {
		return $this->prevent_pos;
	}

	/**
	 * @param bool $prevent_pos
	 */
	public function setPreventPos(bool $prevent_pos) {
		$this->prevent_pos = $prevent_pos;
	}

	/**
	 * @return bool
	 */
	public function isTransporter(): bool {
		return $this->is_transporter;
	}

	/**
	 * @param bool $is_transporter
	 */
	public function setIsTransporter(bool $is_transporter) {
		$this->is_transporter = $is_transporter;
	}

	/**
	 * @return string
	 */
	public function getCreation(): string {
		return $this->creation;
	}

	/**
	 * @param string $creation
	 */
	public function setCreation(string $creation) {
		$this->creation = $creation;
	}

	/**
	 * @return string
	 */
	public function getDoctype(): string {
		return $this->doctype;
	}

	/**
	 * @param string $doctype
	 */
	public function setDoctype(string $doctype) {
		$this->doctype = $doctype;
	}

	/**
	 * @return bool
	 */
	public function isDisabled(): bool {
		return $this->disabled;
	}

	/**
	 * @param bool $disabled
	 */
	public function setDisabled(bool $disabled) {
		$this->disabled = $disabled;
	}

	/**
	 * @return string
	 */
	public function getAccounts(): string {
		return $this->accounts;
	}

	/**
	 * @param string $accounts
	 */
	public function setAccounts(string $accounts) {
		$this->accounts = $accounts;
	}

	/**
	 * @return bool
	 */
	public function isWarnPos(): bool {
		return $this->warn_pos;
	}

	/**
	 * @param bool $warn_pos
	 */
	public function setWarnPos(bool $warn_pos) {
		$this->warn_pos = $warn_pos;
	}

	/**
	 * @return string
	 */
	public function getOwner(): string {
		return $this->owner;
	}

	/**
	 * @param string $owner
	 */
	public function setOwner(string $owner) {
		$this->owner = $owner;
	}

	/**
	 * @return string
	 */
	public function getHoldType(): string {
		return $this->hold_type;
	}

	/**
	 * @param string $hold_type
	 */
	public function setHoldType(string $hold_type) {
		$this->hold_type = $hold_type;
	}

	/**
	 * @return bool
	 */
	public function isOnHold(): bool {
		return $this->on_hold;
	}

	/**
	 * @param bool $on_hold
	 */
	public function setOnHold(bool $on_hold) {
		$this->on_hold = $on_hold;
	}

	/**
	 * @return bool
	 */
	public function isInternalSupplier(): bool {
		return $this->is_internal_supplier;
	}

	/**
	 * @param bool $is_internal_supplier
	 */
	public function setIsInternalSupplier(bool $is_internal_supplier) {
		$this->is_internal_supplier = $is_internal_supplier;
	}

	/**
	 * @return string
	 */
	public function getSupplierGroup(): string {
		return $this->supplier_group;
	}

	/**
	 * @param string $supplier_group
	 */
	public function setSupplierGroup(string $supplier_group) {
		$this->supplier_group = $supplier_group;
	}

	/**
	 * @return string
	 */
	public function getModifiedBy(): string {
		return $this->modified_by;
	}

	/**
	 * @param string $modified_by
	 */
	public function setModifiedBy(string $modified_by) {
		$this->modified_by = $modified_by;
	}

	/**
	 * @return bool
	 */
	public function isWarnRfqs(): bool {
		return $this->warn_rfqs;
	}

	/**
	 * @param bool $warn_rfqs
	 */
	public function setWarnRfqs(bool $warn_rfqs) {
		$this->warn_rfqs = $warn_rfqs;
	}

	/**
	 * @return string
	 */
	public function getNamingSeries(): string {
		return $this->naming_series;
	}

	/**
	 * @param string $naming_series
	 */
	public function setNamingSeries(string $naming_series) {
		$this->naming_series = $naming_series;
	}

	/**
	 * @return bool
	 */
	public function isDocstatus(): bool {
		return $this->docstatus;
	}

	/**
	 * @param bool $docstatus
	 */
	public function setDocstatus(bool $docstatus) {
		$this->docstatus = $docstatus;
	}

	/**
	 * @return string
	 */
	public function getLanguage(): string {
		return $this->language;
	}

	/**
	 * @param string $language
	 */
	public function setLanguage(string $language) {
		$this->language = $language;
	}

	/**
	 * @return string
	 */
	public function getSupplierName(): string {
		return $this->supplier_name;
	}

	/**
	 * @param string $supplier_name
	 */
	public function setSupplierName(string $supplier_name) {
		$this->supplier_name = $supplier_name;
	}

	/**
	 * @return string
	 */
	public function getSupplierType(): string {
		return $this->supplier_type;
	}

	/**
	 * @param string $supplier_type
	 */
	public function setSupplierType(string $supplier_type) {
		$this->supplier_type = $supplier_type;
	}

	/**
	 * @return string
	 */
	public function getName(): string {
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName(string $name) {
		$this->name = $name;
	}

	/**
	 * @return int
	 */
	public function getIdx(): int {
		return $this->idx;
	}

	/**
	 * @param int $idx
	 */
	public function setIdx(int $idx) {
		$this->idx = $idx;
	}

	/**
	 * @return bool
	 */
	public function isPreventRfqs(): bool {
		return $this->prevent_rfqs;
	}

	/**
	 * @param bool $prevent_rfqs
	 */
	public function setPreventRfqs(bool $prevent_rfqs) {
		$this->prevent_rfqs = $prevent_rfqs;
	}

	/**
	 * @return string
	 */
	public function getCountry(): string {
		return $this->country;
	}

	/**
	 * @param string $country
	 */
	public function setCountry(string $country) {
		$this->country = $country;
	}

	/**
	 * @return string
	 */
	public function getModified(): string {
		return $this->modified;
	}

	/**
	 * @param string $modified
	 */
	public function setModified(string $modified) {
		$this->modified = $modified;
	}

	/**
	 * @return bool
	 */
	public function isFrozen(): bool {
		return $this->is_frozen;
	}

	/**
	 * @param bool $is_frozen
	 */
	public function setIsFrozen(bool $is_frozen) {
		$this->is_frozen = $is_frozen;
	}

}
