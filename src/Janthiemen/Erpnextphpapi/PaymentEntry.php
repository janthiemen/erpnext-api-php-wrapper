<?php
/**
 * (c) Jan Thiemen <jan.thiemen@euroskano.nl>
 *
 * This source file is subject to the GNU General Public License (v3) that is bundled
 * with this source code in the file LICENSE.
 */

namespace Janthiemen\Erpnextphpapi;

class PaymentEntry extends ERPNextObject {

	/** @var $total_allocated_amount string */
	protected $total_allocated_amount;
	/** @var $naming_series string */
	protected $naming_series;
	/** @var $mode_of_payment string */
	protected $mode_of_payment;
	/** @var $target_exchange_rate string */
	protected $target_exchange_rate;
	/** @var $paid_to string */
	protected $paid_to;
	/** @var $base_paid_amount string */
	protected $base_paid_amount;
	/** @var $paid_to_account_currency string */
	protected $paid_to_account_currency;
	/** @var $reference_date string */
	protected $reference_date;
	/** @var $reference_no string */
	protected $reference_no;
	/** @var $references array */
	protected $references = [];
	/** @var $owner string */
	protected $owner;
	/** @var $camt_amount string */
	protected $camt_amount;
	/** @var $unallocated_amount string */
	protected $unallocated_amount;
	/** @var $allocate_payment_amount int */
	protected $allocate_payment_amount;
	/** @var $modified_by string */
	protected $modified_by;
	/** @var $paid_amount string */
	protected $paid_amount;
	/** @var $title string */
	protected $title;
	/** @var $party_type string */
	protected $party_type;
	/** @var $base_total_allocated_amount string */
	protected $base_total_allocated_amount;
	/** @var $remarks string */
	protected $remarks;
	/** @var $party string */
	protected $party;
	/** @var $base_received_amount string */
	protected $base_received_amount;
	/** @var $source_exchange_rate string */
	protected $source_exchange_rate;
	/** @var $creation string */
	protected $creation;
	/** @var $doctype string */
	protected $doctype;
	/** @var $paid_from_account_balance string */
	protected $paid_from_account_balance;
	/** @var $company string */
	protected $company;
	/** @var $paid_from string */
	protected $paid_from;
	/** @var $party_balance string */
	protected $party_balance;
	/** @var $deductions array */
	protected $deductions = [];
	/** @var $party_name string */
	protected $party_name;
	/** @var $docstatus int */
	protected $docstatus;
	/** @var $paid_from_account_currency string */
	protected $paid_from_account_currency;
	/** @var $paid_to_account_balance string */
	protected $paid_to_account_balance;
	/** @var $name string */
	protected $name;
	/** @var $idx int */
	protected $idx;
	/** @var $difference_amount string */
	protected $difference_amount;
	/** @var $modified string */
	protected $modified;
	/** @var $received_amount string */
	protected $received_amount;
	/** @var $payment_type string */
	protected $payment_type;
	/** @var $transaction_type string */
	protected $transaction_type;
	/** @var $posting_date string */
	protected $posting_date;
	/** @var $exported_to_abacus int */
	protected $exported_to_abacus;

	/**
	 * Constructor for the ERPNext Sales Invoice model
	 * @param $data array The data array as retrieved from ERP Next
	 */
	public function __construct($data = null) {
		parent::__construct($data);
		if (!is_null($data) && isset($data->items)) {
			$this->references = [];
			foreach ($data->references as $reference) {
				$this->references[] = new Reference($reference);
			}
		}
	}

	public function __toString() {
		$data = $this->__toArray();
		$data["references"] = [];
		/** @var InvoiceItem $item */
		foreach ($this->references as $reference) {
			$data["references"][] = $reference->__toArray();
		}
		return json_encode($data, JSON_NUMERIC_CHECK);
	}

	/**
	 * @return string
	 */
	public function getTotalAllocatedAmount(): ?string {
		return $this->total_allocated_amount;
	}

	/**
	 * @param string $total_allocated_amount
	 */
	public function setTotalAllocatedAmount(string $total_allocated_amount) {
		$this->total_allocated_amount = $total_allocated_amount;
	}

	/**
	 * @return string
	 */
	public function getNamingSeries(): ?string {
		return $this->naming_series;
	}

	/**
	 * @param string $naming_series
	 */
	public function setNamingSeries(string $naming_series) {
		$this->naming_series = $naming_series;
	}

	/**
	 * @return string
	 */
	public function getModeOfPayment(): ?string {
		return $this->mode_of_payment;
	}

	/**
	 * @param string $mode_of_payment
	 */
	public function setModeOfPayment(string $mode_of_payment) {
		$this->mode_of_payment = $mode_of_payment;
	}

	/**
	 * @return string
	 */
	public function getTargetExchangeRate(): ?string {
		return $this->target_exchange_rate;
	}

	/**
	 * @param string $target_exchange_rate
	 */
	public function setTargetExchangeRate(string $target_exchange_rate) {
		$this->target_exchange_rate = $target_exchange_rate;
	}

	/**
	 * @return string
	 */
	public function getPaidTo(): ?string {
		return $this->paid_to;
	}

	/**
	 * @param string $paid_to
	 */
	public function setPaidTo(string $paid_to) {
		$this->paid_to = $paid_to;
	}

	/**
	 * @return string
	 */
	public function getBasePaidAmount(): ?string {
		return $this->base_paid_amount;
	}

	/**
	 * @param string $base_paid_amount
	 */
	public function setBasePaidAmount(string $base_paid_amount) {
		$this->base_paid_amount = $base_paid_amount;
	}

	/**
	 * @return string
	 */
	public function getPaidToAccountCurrency(): ?string {
		return $this->paid_to_account_currency;
	}

	/**
	 * @param string $paid_to_account_currency
	 */
	public function setPaidToAccountCurrency(string $paid_to_account_currency) {
		$this->paid_to_account_currency = $paid_to_account_currency;
	}

	/**
	 * @return string
	 */
	public function getReferenceDate(): ?string {
		return $this->reference_date;
	}

	/**
	 * @param string $reference_date
	 */
	public function setReferenceDate(string $reference_date) {
		$this->reference_date = $reference_date;
	}

	/**
	 * @return string
	 */
	public function getReferenceNo(): ?string {
		return $this->reference_no;
	}

	/**
	 * @param string $reference_no
	 */
	public function setReferenceNo(string $reference_no) {
		$this->reference_no = $reference_no;
	}

	/**
	 * @return array
	 */
	public function getReferences(): array {
		return $this->references;
	}

	/**
	 * @param array $references
	 */
	public function setReferences(array $references) {
		$this->references = $references;
	}

	/**
	 * @param PaymentEntryReference $reference
	 */
	public function addReference(PaymentEntryReference $reference) {
		$this->references[] = $reference;
	}

	/**
	 * @return string
	 */
	public function getOwner(): ?string {
		return $this->owner;
	}

	/**
	 * @param string $owner
	 */
	public function setOwner(string $owner) {
		$this->owner = $owner;
	}

	/**
	 * @return string
	 */
	public function getCamtAmount(): ?string {
		return $this->camt_amount;
	}

	/**
	 * @param string $camt_amount
	 */
	public function setCamtAmount(string $camt_amount) {
		$this->camt_amount = $camt_amount;
	}

	/**
	 * @return string
	 */
	public function getUnallocatedAmount(): ?string {
		return $this->unallocated_amount;
	}

	/**
	 * @param string $unallocated_amount
	 */
	public function setUnallocatedAmount(string $unallocated_amount) {
		$this->unallocated_amount = $unallocated_amount;
	}

	/**
	 * @return int
	 */
	public function getAllocatePaymentAmount(): ?int {
		return $this->allocate_payment_amount;
	}

	/**
	 * @param int $allocate_payment_amount
	 */
	public function setAllocatePaymentAmount(int $allocate_payment_amount) {
		$this->allocate_payment_amount = $allocate_payment_amount;
	}

	/**
	 * @return string
	 */
	public function getModifiedBy(): ?string {
		return $this->modified_by;
	}

	/**
	 * @param string $modified_by
	 */
	public function setModifiedBy(string $modified_by) {
		$this->modified_by = $modified_by;
	}

	/**
	 * @return string
	 */
	public function getPaidAmount(): ?string {
		return $this->paid_amount;
	}

	/**
	 * @param string $paid_amount
	 */
	public function setPaidAmount(string $paid_amount) {
		$this->paid_amount = $paid_amount;
	}

	/**
	 * @return string
	 */
	public function getTitle(): ?string {
		return $this->title;
	}

	/**
	 * @param string $title
	 */
	public function setTitle(string $title) {
		$this->title = $title;
	}

	/**
	 * @return string
	 */
	public function getPartyType(): ?string {
		return $this->party_type;
	}

	/**
	 * @param string $party_type
	 */
	public function setPartyType(string $party_type) {
		$this->party_type = $party_type;
	}

	/**
	 * @return string
	 */
	public function getBaseTotalAllocatedAmount(): ?string {
		return $this->base_total_allocated_amount;
	}

	/**
	 * @param string $base_total_allocated_amount
	 */
	public function setBaseTotalAllocatedAmount(string $base_total_allocated_amount) {
		$this->base_total_allocated_amount = $base_total_allocated_amount;
	}

	/**
	 * @return string
	 */
	public function getRemarks(): ?string {
		return $this->remarks;
	}

	/**
	 * @param string $remarks
	 */
	public function setRemarks(string $remarks) {
		$this->remarks = $remarks;
	}

	/**
	 * @return string
	 */
	public function getParty(): ?string {
		return $this->party;
	}

	/**
	 * @param string $party
	 */
	public function setParty(string $party) {
		$this->party = $party;
	}

	/**
	 * @return string
	 */
	public function getBaseReceivedAmount(): ?string {
		return $this->base_received_amount;
	}

	/**
	 * @param string $base_received_amount
	 */
	public function setBaseReceivedAmount(string $base_received_amount) {
		$this->base_received_amount = $base_received_amount;
	}

	/**
	 * @return string
	 */
	public function getSourceExchangeRate(): ?string {
		return $this->source_exchange_rate;
	}

	/**
	 * @param string $source_exchange_rate
	 */
	public function setSourceExchangeRate(string $source_exchange_rate) {
		$this->source_exchange_rate = $source_exchange_rate;
	}

	/**
	 * @return string
	 */
	public function getCreation(): ?string {
		return $this->creation;
	}

	/**
	 * @param string $creation
	 */
	public function setCreation(string $creation) {
		$this->creation = $creation;
	}

	/**
	 * @return string
	 */
	public function getDoctype(): ?string {
		return $this->doctype;
	}

	/**
	 * @param string $doctype
	 */
	public function setDoctype(string $doctype) {
		$this->doctype = $doctype;
	}

	/**
	 * @return string
	 */
	public function getPaidFromAccountBalance(): ?string {
		return $this->paid_from_account_balance;
	}

	/**
	 * @param string $paid_from_account_balance
	 */
	public function setPaidFromAccountBalance(string $paid_from_account_balance) {
		$this->paid_from_account_balance = $paid_from_account_balance;
	}

	/**
	 * @return string
	 */
	public function getCompany(): ?string {
		return $this->company;
	}

	/**
	 * @param string $company
	 */
	public function setCompany(string $company) {
		$this->company = $company;
	}

	/**
	 * @return string
	 */
	public function getPaidFrom(): ?string {
		return $this->paid_from;
	}

	/**
	 * @param string $paid_from
	 */
	public function setPaidFrom(string $paid_from) {
		$this->paid_from = $paid_from;
	}

	/**
	 * @return string
	 */
	public function getPartyBalance(): ?string {
		return $this->party_balance;
	}

	/**
	 * @param string $party_balance
	 */
	public function setPartyBalance(string $party_balance) {
		$this->party_balance = $party_balance;
	}

	/**
	 * @return array
	 */
	public function getDeductions(): array {
		return $this->deductions;
	}

	/**
	 * @param array $deductions
	 */
	public function setDeductions(array $deductions) {
		$this->deductions = $deductions;
	}

	/**
	 * @return string
	 */
	public function getPartyName(): ?string {
		return $this->party_name;
	}

	/**
	 * @param string $party_name
	 */
	public function setPartyName(string $party_name) {
		$this->party_name = $party_name;
	}

	/**
	 * @return int
	 */
	public function getDocstatus(): ?int {
		return $this->docstatus;
	}

	/**
	 * @param int $docstatus
	 */
	public function setDocstatus(int $docstatus) {
		$this->docstatus = $docstatus;
	}

	/**
	 * @return string
	 */
	public function getPaidFromAccountCurrency(): ?string {
		return $this->paid_from_account_currency;
	}

	/**
	 * @param string $paid_from_account_currency
	 */
	public function setPaidFromAccountCurrency(string $paid_from_account_currency) {
		$this->paid_from_account_currency = $paid_from_account_currency;
	}

	/**
	 * @return string
	 */
	public function getPaidToAccountBalance(): ?string {
		return $this->paid_to_account_balance;
	}

	/**
	 * @param string $paid_to_account_balance
	 */
	public function setPaidToAccountBalance(string $paid_to_account_balance) {
		$this->paid_to_account_balance = $paid_to_account_balance;
	}

	/**
	 * @return string
	 */
	public function getName(): ?string {
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName(string $name) {
		$this->name = $name;
	}

	/**
	 * @return int
	 */
	public function getIdx(): ?int {
		return $this->idx;
	}

	/**
	 * @param int $idx
	 */
	public function setIdx(int $idx) {
		$this->idx = $idx;
	}

	/**
	 * @return string
	 */
	public function getDifferenceAmount(): ?string {
		return $this->difference_amount;
	}

	/**
	 * @param string $difference_amount
	 */
	public function setDifferenceAmount(string $difference_amount) {
		$this->difference_amount = $difference_amount;
	}

	/**
	 * @return string
	 */
	public function getModified(): ?string {
		return $this->modified;
	}

	/**
	 * @param string $modified
	 */
	public function setModified(string $modified) {
		$this->modified = $modified;
	}

	/**
	 * @return string
	 */
	public function getReceivedAmount(): ?string {
		return $this->received_amount;
	}

	/**
	 * @param string $received_amount
	 */
	public function setReceivedAmount(string $received_amount) {
		$this->received_amount = $received_amount;
	}

	/**
	 * @return string
	 */
	public function getPaymentType(): ?string {
		return $this->payment_type;
	}

	/**
	 * @param string $payment_type
	 */
	public function setPaymentType(string $payment_type) {
		$this->payment_type = $payment_type;
	}

	/**
	 * @return string
	 */
	public function getTransactionType(): ?string {
		return $this->transaction_type;
	}

	/**
	 * @param string $transaction_type
	 */
	public function setTransactionType(string $transaction_type) {
		$this->transaction_type = $transaction_type;
	}

	/**
	 * @return string
	 */
	public function getPostingDate(): ?string {
		return $this->posting_date;
	}

	/**
	 * @param string $posting_date
	 */
	public function setPostingDate(string $posting_date) {
		$this->posting_date = $posting_date;
	}

	/**
	 * @return int
	 */
	public function getExportedToAbacus(): ?int {
		return $this->exported_to_abacus;
	}

	/**
	 * @param int $exported_to_abacus
	 */
	public function setExportedToAbacus(int $exported_to_abacus) {
		$this->exported_to_abacus = $exported_to_abacus;
	}
}
