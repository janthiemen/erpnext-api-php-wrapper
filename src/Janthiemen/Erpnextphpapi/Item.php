<?php
/**
 * (c) Jan Thiemen <jan.thiemen@euroskano.nl>
 *
 * This source file is subject to the GNU General Public License (v3) that is bundled
 * with this source code in the file LICENSE.
 */

namespace janthiemen\erpnextphpapi;

class Item extends ERPNextObject {

	/** @var $enable_deferred_expense bool */
	protected $enable_deferred_expense;
	/** @var $disabled bool */
	protected $disabled;
	/** @var $modified_by string */
	protected $modified_by;
	/** @var $item_name string */
	protected $item_name;
	/** @var $has_expiry_date int */
	protected $has_expiry_date;
	/** @var $default_material_request_type string */
	protected $default_material_request_type;
	/** @var $is_purchase_item int */
	protected $is_purchase_item;
	/** @var $max_discount string */
	protected $max_discount;
	/** @var $name string */
	protected $name;
	/** @var $item_group string */
	protected $item_group;
	/** @var $weight_per_unit string */
	protected $weight_per_unit;
	/** @var $creation string */
	protected $creation;
	/** @var $doctype string */
	protected $doctype;
	/** @var $create_new_batch int */
	protected $create_new_batch;
	/** @var $has_variants int */
	protected $has_variants;
	/** @var $enable_deferred_revenue int */
	protected $enable_deferred_revenue;
	/** @var $inspection_required_before_delivery int */
	protected $inspection_required_before_delivery;
	/** @var $is_sales_item int */
	protected $is_sales_item;
	/** @var $is_sub_contracted_item int */
	protected $is_sub_contracted_item;
	/** @var $shelf_life_in_days int */
	protected $shelf_life_in_days;
	/** @var $tolerance string */
	protected $tolerance;
	/** @var $customer_code string */
	protected $customer_code;
	/** @var $is_stock_item bool */
	protected $is_stock_item;
	/** @var $idx int */
	protected $idx;
	/** @var $variant_based_on string */
	protected $variant_based_on;
	/** @var $min_order_qty string */
	protected $min_order_qty;
	/** @var $valuation_rate string */
	protected $valuation_rate;
	/** @var $owner string */
	protected $owner;
	/** @var $is_customer_provided_item bool */
	protected $is_customer_provided_item;
	/** @var $country_of_origin string */
	protected $country_of_origin;
	/** @var $is_item_from_hub bool */
	protected $is_item_from_hub;
	/** @var $valuation_method string */
	protected $valuation_method;
	/** @var $item_code string */
	protected $item_code;
	/** @var $retain_sample int */
	protected $retain_sample;
	/** @var $show_in_website int */
	protected $show_in_website;
	/** @var $safety_stock string */
	protected $safety_stock;
	/** @var $modified string */
	protected $modified;
	/** @var $delivered_by_supplier int */
	protected $delivered_by_supplier;
	/** @var $last_purchase_rate string */
	protected $last_purchase_rate;
	/** @var $naming_series string */
	protected $naming_series;
	/** @var $publish_in_hub int */
	protected $publish_in_hub;
	/** @var $allow_alternative_item bool */
	protected $allow_alternative_item;
	/** @var $include_item_in_manufacturing bool */
	protected $include_item_in_manufacturing;
	/** @var $no_of_months int */
	protected $no_of_months;
	/** @var $end_of_life string */
	protected $end_of_life;
	/** @var $synced_with_hub int */
	protected $synced_with_hub;
	/** @var $stock_uom string */
	protected $stock_uom;
	/** @var $show_variant_in_website int */
	protected $show_variant_in_website;
	/** @var $docstatus int */
	protected $docstatus;
	/** @var $sample_quantity int */
	protected $sample_quantity;
	/** @var $description string */
	protected $description;
	/** @var $weightage int */
	protected $weightage;
	/** @var $no_of_months_exp int */
	protected $no_of_months_exp;
	/** @var $total_projected_qty string */
	protected $total_projected_qty;
	/** @var $lead_time_days int */
	protected $lead_time_days;
	/** @var $opening_stock string */
	protected $opening_stock;
	/** @var $has_batch_no int */
	protected $has_batch_no;
	/** @var $has_serial_no int */
	protected $has_serial_no;
	/** @var $standard_rate string */
	protected $standard_rate;
	/** @var $is_fixed_asset bool */
	protected $is_fixed_asset;
	/** @var $inspection_required_before_purchase int */
	protected $inspection_required_before_purchase;

	/**
	 * Constructor for the ERPNext Item model
	 * @param $data array The data array as retrieved from ERP Next
	 */
	public function __construct($data = null) {
		parent::__construct($data);
	}

	/**
	 * @return bool
	 */
	public function isEnableDeferredExpense(): bool {
		return $this->enable_deferred_expense;
	}

	/**
	 * @param bool $enable_deferred_expense
	 */
	public function setEnableDeferredExpense(bool $enable_deferred_expense) {
		$this->enable_deferred_expense = $enable_deferred_expense;
	}

	/**
	 * @return bool
	 */
	public function isDisabled(): bool {
		return $this->disabled;
	}

	/**
	 * @param bool $disabled
	 */
	public function setDisabled(bool $disabled) {
		$this->disabled = $disabled;
	}

	/**
	 * @return string
	 */
	public function getModifiedBy(): ?string {
		return $this->modified_by;
	}

	/**
	 * @param string $modified_by
	 */
	public function setModifiedBy(string $modified_by) {
		$this->modified_by = $modified_by;
	}

	/**
	 * @return string
	 */
	public function getItemName(): ?string {
		return $this->item_name;
	}

	/**
	 * @param string $item_name
	 */
	public function setItemName(string $item_name) {
		$this->item_name = $item_name;
	}

	/**
	 * @return int
	 */
	public function getHasExpiryDate(): int {
		return $this->has_expiry_date;
	}

	/**
	 * @param int $has_expiry_date
	 */
	public function setHasExpiryDate(int $has_expiry_date) {
		$this->has_expiry_date = $has_expiry_date;
	}

	/**
	 * @return string
	 */
	public function getDefaultMaterialRequestType(): ?string {
		return $this->default_material_request_type;
	}

	/**
	 * @param string $default_material_request_type
	 */
	public function setDefaultMaterialRequestType(string $default_material_request_type) {
		$this->default_material_request_type = $default_material_request_type;
	}

	/**
	 * @return int
	 */
	public function getisPurchaseItem(): int {
		return $this->is_purchase_item;
	}

	/**
	 * @param int $is_purchase_item
	 */
	public function setIsPurchaseItem(int $is_purchase_item) {
		$this->is_purchase_item = $is_purchase_item;
	}

	/**
	 * @return string
	 */
	public function getMaxDiscount(): ?string {
		return $this->max_discount;
	}

	/**
	 * @param string $max_discount
	 */
	public function setMaxDiscount(string $max_discount) {
		$this->max_discount = $max_discount;
	}

	/**
	 * @return string
	 */
	public function getName(): ?string {
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName(string $name) {
		$this->name = $name;
	}

	/**
	 * @return string
	 */
	public function getItemGroup(): ?string {
		return $this->item_group;
	}

	/**
	 * @param string $item_group
	 */
	public function setItemGroup(string $item_group) {
		$this->item_group = $item_group;
	}

	/**
	 * @return string
	 */
	public function getWeightPerUnit(): ?string {
		return $this->weight_per_unit;
	}

	/**
	 * @param string $weight_per_unit
	 */
	public function setWeightPerUnit(string $weight_per_unit) {
		$this->weight_per_unit = $weight_per_unit;
	}

	/**
	 * @return string
	 */
	public function getCreation(): ?string {
		return $this->creation;
	}

	/**
	 * @param string $creation
	 */
	public function setCreation(string $creation) {
		$this->creation = $creation;
	}

	/**
	 * @return string
	 */
	public function getDoctype(): ?string {
		return $this->doctype;
	}

	/**
	 * @param string $doctype
	 */
	public function setDoctype(string $doctype) {
		$this->doctype = $doctype;
	}

	/**
	 * @return int
	 */
	public function getCreateNewBatch(): int {
		return $this->create_new_batch;
	}

	/**
	 * @param int $create_new_batch
	 */
	public function setCreateNewBatch(int $create_new_batch) {
		$this->create_new_batch = $create_new_batch;
	}

	/**
	 * @return int
	 */
	public function getHasVariants(): int {
		return $this->has_variants;
	}

	/**
	 * @param int $has_variants
	 */
	public function setHasVariants(int $has_variants) {
		$this->has_variants = $has_variants;
	}

	/**
	 * @return int
	 */
	public function getEnableDeferredRevenue(): int {
		return $this->enable_deferred_revenue;
	}

	/**
	 * @param int $enable_deferred_revenue
	 */
	public function setEnableDeferredRevenue(int $enable_deferred_revenue) {
		$this->enable_deferred_revenue = $enable_deferred_revenue;
	}

	/**
	 * @return int
	 */
	public function getInspectionRequiredBeforeDelivery(): int {
		return $this->inspection_required_before_delivery;
	}

	/**
	 * @param int $inspection_required_before_delivery
	 */
	public function setInspectionRequiredBeforeDelivery(int $inspection_required_before_delivery) {
		$this->inspection_required_before_delivery = $inspection_required_before_delivery;
	}

	/**
	 * @return int
	 */
	public function getisSalesItem(): int {
		return $this->is_sales_item;
	}

	/**
	 * @param int $is_sales_item
	 */
	public function setIsSalesItem(int $is_sales_item) {
		$this->is_sales_item = $is_sales_item;
	}

	/**
	 * @return int
	 */
	public function getisSubContractedItem(): int {
		return $this->is_sub_contracted_item;
	}

	/**
	 * @param int $is_sub_contracted_item
	 */
	public function setIsSubContractedItem(int $is_sub_contracted_item) {
		$this->is_sub_contracted_item = $is_sub_contracted_item;
	}

	/**
	 * @return int
	 */
	public function getShelfLifeInDays(): int {
		return $this->shelf_life_in_days;
	}

	/**
	 * @param int $shelf_life_in_days
	 */
	public function setShelfLifeInDays(int $shelf_life_in_days) {
		$this->shelf_life_in_days = $shelf_life_in_days;
	}

	/**
	 * @return string
	 */
	public function getTolerance(): ?string {
		return $this->tolerance;
	}

	/**
	 * @param string $tolerance
	 */
	public function setTolerance(string $tolerance) {
		$this->tolerance = $tolerance;
	}

	/**
	 * @return string
	 */
	public function getCustomerCode(): ?string {
		return $this->customer_code;
	}

	/**
	 * @param string $customer_code
	 */
	public function setCustomerCode(string $customer_code) {
		$this->customer_code = $customer_code;
	}

	/**
	 * @return bool
	 */
	public function isStockItem(): bool {
		return $this->is_stock_item;
	}

	/**
	 * @param bool $is_stock_item
	 */
	public function setIsStockItem(bool $is_stock_item) {
		$this->is_stock_item = $is_stock_item;
	}

	/**
	 * @return int
	 */
	public function getIdx(): int {
		return $this->idx;
	}

	/**
	 * @param int $idx
	 */
	public function setIdx(int $idx) {
		$this->idx = $idx;
	}

	/**
	 * @return string
	 */
	public function getVariantBasedOn(): ?string {
		return $this->variant_based_on;
	}

	/**
	 * @param string $variant_based_on
	 */
	public function setVariantBasedOn(string $variant_based_on) {
		$this->variant_based_on = $variant_based_on;
	}

	/**
	 * @return string
	 */
	public function getMinOrderQty(): ?string {
		return $this->min_order_qty;
	}

	/**
	 * @param string $min_order_qty
	 */
	public function setMinOrderQty(string $min_order_qty) {
		$this->min_order_qty = $min_order_qty;
	}

	/**
	 * @return string
	 */
	public function getValuationRate(): ?string {
		return $this->valuation_rate;
	}

	/**
	 * @param string $valuation_rate
	 */
	public function setValuationRate(string $valuation_rate) {
		$this->valuation_rate = $valuation_rate;
	}

	/**
	 * @return string
	 */
	public function getOwner(): ?string {
		return $this->owner;
	}

	/**
	 * @param string $owner
	 */
	public function setOwner(string $owner) {
		$this->owner = $owner;
	}

	/**
	 * @return bool
	 */
	public function isCustomerProvidedItem(): bool {
		return $this->is_customer_provided_item;
	}

	/**
	 * @param bool $is_customer_provided_item
	 */
	public function setIsCustomerProvidedItem(bool $is_customer_provided_item) {
		$this->is_customer_provided_item = $is_customer_provided_item;
	}

	/**
	 * @return string
	 */
	public function getCountryOfOrigin(): ?string {
		return $this->country_of_origin;
	}

	/**
	 * @param string $country_of_origin
	 */
	public function setCountryOfOrigin(string $country_of_origin) {
		$this->country_of_origin = $country_of_origin;
	}

	/**
	 * @return bool
	 */
	public function isItemFromHub(): bool {
		return $this->is_item_from_hub;
	}

	/**
	 * @param bool $is_item_from_hub
	 */
	public function setIsItemFromHub(bool $is_item_from_hub) {
		$this->is_item_from_hub = $is_item_from_hub;
	}

	/**
	 * @return string
	 */
	public function getValuationMethod(): ?string {
		return $this->valuation_method;
	}

	/**
	 * @param string $valuation_method
	 */
	public function setValuationMethod(string $valuation_method) {
		$this->valuation_method = $valuation_method;
	}

	/**
	 * @return string
	 */
	public function getItemCode(): ?string {
		return $this->item_code;
	}

	/**
	 * @param string $item_code
	 */
	public function setItemCode(string $item_code) {
		$this->item_code = $item_code;
	}

	/**
	 * @return int
	 */
	public function getRetainSample(): int {
		return $this->retain_sample;
	}

	/**
	 * @param int $retain_sample
	 */
	public function setRetainSample(int $retain_sample) {
		$this->retain_sample = $retain_sample;
	}

	/**
	 * @return int
	 */
	public function getShowInWebsite(): int {
		return $this->show_in_website;
	}

	/**
	 * @param int $show_in_website
	 */
	public function setShowInWebsite(int $show_in_website) {
		$this->show_in_website = $show_in_website;
	}

	/**
	 * @return string
	 */
	public function getSafetyStock(): ?string {
		return $this->safety_stock;
	}

	/**
	 * @param string $safety_stock
	 */
	public function setSafetyStock(string $safety_stock) {
		$this->safety_stock = $safety_stock;
	}

	/**
	 * @return string
	 */
	public function getModified(): ?string {
		return $this->modified;
	}

	/**
	 * @param string $modified
	 */
	public function setModified(string $modified) {
		$this->modified = $modified;
	}

	/**
	 * @return int
	 */
	public function getDeliveredBySupplier(): int {
		return $this->delivered_by_supplier;
	}

	/**
	 * @param int $delivered_by_supplier
	 */
	public function setDeliveredBySupplier(int $delivered_by_supplier) {
		$this->delivered_by_supplier = $delivered_by_supplier;
	}

	/**
	 * @return string
	 */
	public function getLastPurchaseRate(): ?string {
		return $this->last_purchase_rate;
	}

	/**
	 * @param string $last_purchase_rate
	 */
	public function setLastPurchaseRate(string $last_purchase_rate) {
		$this->last_purchase_rate = $last_purchase_rate;
	}

	/**
	 * @return string
	 */
	public function getNamingSeries(): ?string {
		return $this->naming_series;
	}

	/**
	 * @param string $naming_series
	 */
	public function setNamingSeries(string $naming_series) {
		$this->naming_series = $naming_series;
	}

	/**
	 * @return int
	 */
	public function getPublishInHub(): int {
		return $this->publish_in_hub;
	}

	/**
	 * @param int $publish_in_hub
	 */
	public function setPublishInHub(int $publish_in_hub) {
		$this->publish_in_hub = $publish_in_hub;
	}

	/**
	 * @return bool
	 */
	public function isAllowAlternativeItem(): bool {
		return $this->allow_alternative_item;
	}

	/**
	 * @param bool $allow_alternative_item
	 */
	public function setAllowAlternativeItem(bool $allow_alternative_item) {
		$this->allow_alternative_item = $allow_alternative_item;
	}

	/**
	 * @return bool
	 */
	public function isIncludeItemInManufacturing(): bool {
		return $this->include_item_in_manufacturing;
	}

	/**
	 * @param bool $include_item_in_manufacturing
	 */
	public function setIncludeItemInManufacturing(bool $include_item_in_manufacturing) {
		$this->include_item_in_manufacturing = $include_item_in_manufacturing;
	}

	/**
	 * @return int
	 */
	public function getNoOfMonths(): int {
		return $this->no_of_months;
	}

	/**
	 * @param int $no_of_months
	 */
	public function setNoOfMonths(int $no_of_months) {
		$this->no_of_months = $no_of_months;
	}

	/**
	 * @return string
	 */
	public function getEndOfLife(): ?string {
		return $this->end_of_life;
	}

	/**
	 * @param string $end_of_life
	 */
	public function setEndOfLife(string $end_of_life) {
		$this->end_of_life = $end_of_life;
	}

	/**
	 * @return int
	 */
	public function getSyncedWithHub(): int {
		return $this->synced_with_hub;
	}

	/**
	 * @param int $synced_with_hub
	 */
	public function setSyncedWithHub(int $synced_with_hub) {
		$this->synced_with_hub = $synced_with_hub;
	}

	/**
	 * @return string
	 */
	public function getStockUom(): ?string {
		return $this->stock_uom;
	}

	/**
	 * @param string $stock_uom
	 */
	public function setStockUom(string $stock_uom) {
		$this->stock_uom = $stock_uom;
	}

	/**
	 * @return int
	 */
	public function getShowVariantInWebsite(): int {
		return $this->show_variant_in_website;
	}

	/**
	 * @param int $show_variant_in_website
	 */
	public function setShowVariantInWebsite(int $show_variant_in_website) {
		$this->show_variant_in_website = $show_variant_in_website;
	}

	/**
	 * @return int
	 */
	public function getDocstatus(): int {
		return $this->docstatus;
	}

	/**
	 * @param int $docstatus
	 */
	public function setDocstatus(int $docstatus) {
		$this->docstatus = $docstatus;
	}

	/**
	 * @return int
	 */
	public function getSampleQuantity(): int {
		return $this->sample_quantity;
	}

	/**
	 * @param int $sample_quantity
	 */
	public function setSampleQuantity(int $sample_quantity) {
		$this->sample_quantity = $sample_quantity;
	}

	/**
	 * @return string
	 */
	public function getDescription(): ?string {
		return $this->description;
	}

	/**
	 * @param string $description
	 */
	public function setDescription(string $description) {
		$this->description = $description;
	}

	/**
	 * @return int
	 */
	public function getWeightage(): int {
		return $this->weightage;
	}

	/**
	 * @param int $weightage
	 */
	public function setWeightage(int $weightage) {
		$this->weightage = $weightage;
	}

	/**
	 * @return int
	 */
	public function getNoOfMonthsExp(): int {
		return $this->no_of_months_exp;
	}

	/**
	 * @param int $no_of_months_exp
	 */
	public function setNoOfMonthsExp(int $no_of_months_exp) {
		$this->no_of_months_exp = $no_of_months_exp;
	}

	/**
	 * @return string
	 */
	public function getTotalProjectedQty(): ?string {
		return $this->total_projected_qty;
	}

	/**
	 * @param string $total_projected_qty
	 */
	public function setTotalProjectedQty(string $total_projected_qty) {
		$this->total_projected_qty = $total_projected_qty;
	}

	/**
	 * @return int
	 */
	public function getLeadTimeDays(): int {
		return $this->lead_time_days;
	}

	/**
	 * @param int $lead_time_days
	 */
	public function setLeadTimeDays(int $lead_time_days) {
		$this->lead_time_days = $lead_time_days;
	}

	/**
	 * @return string
	 */
	public function getOpeningStock(): ?string {
		return $this->opening_stock;
	}

	/**
	 * @param string $opening_stock
	 */
	public function setOpeningStock(string $opening_stock) {
		$this->opening_stock = $opening_stock;
	}

	/**
	 * @return int
	 */
	public function getHasBatchNo(): int {
		return $this->has_batch_no;
	}

	/**
	 * @param int $has_batch_no
	 */
	public function setHasBatchNo(int $has_batch_no) {
		$this->has_batch_no = $has_batch_no;
	}

	/**
	 * @return int
	 */
	public function getHasSerialNo(): int {
		return $this->has_serial_no;
	}

	/**
	 * @param int $has_serial_no
	 */
	public function setHasSerialNo(int $has_serial_no) {
		$this->has_serial_no = $has_serial_no;
	}

	/**
	 * @return string
	 */
	public function getStandardRate(): ?string {
		return $this->standard_rate;
	}

	/**
	 * @param string $standard_rate
	 */
	public function setStandardRate(string $standard_rate) {
		$this->standard_rate = $standard_rate;
	}

	/**
	 * @return bool
	 */
	public function isFixedAsset(): bool {
		return $this->is_fixed_asset;
	}

	/**
	 * @param bool $is_fixed_asset
	 */
	public function setIsFixedAsset(bool $is_fixed_asset) {
		$this->is_fixed_asset = $is_fixed_asset;
	}

	/**
	 * @return int
	 */
	public function getInspectionRequiredBeforePurchase(): int {
		return $this->inspection_required_before_purchase;
	}

	/**
	 * @param int $inspection_required_before_purchase
	 */
	public function setInspectionRequiredBeforePurchase(int $inspection_required_before_purchase) {
		$this->inspection_required_before_purchase = $inspection_required_before_purchase;
	}

}
