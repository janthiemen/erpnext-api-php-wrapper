<?php
/**
 * (c) Jan Thiemen <jan.thiemen@euroskano.nl>
 *
 * This source file is subject to the GNU General Public License (v3) that is bundled
 * with this source code in the file LICENSE.
 */

namespace Janthiemen\Erpnextphpapi;

class PurchaseInvoice extends ERPNextObject {

	/** @var $base_write_off_amount string */
	protected $base_write_off_amount;
	/** @var $modified_by string */
	protected $modified_by;
	/** @var $title string */
	protected $title;
	/** @var $set_posting_time int */
	protected $set_posting_time;
	/** @var $taxes_and_charges_added string */
	protected $taxes_and_charges_added;
	/** @var $base_in_words string */
	protected $base_in_words;
	/** @var $due_date string */
	protected $due_date;
	/** @var $doctype string */
	protected $doctype;
	/** @var $ignore_pricing_rule int */
	protected $ignore_pricing_rule;
	/** @var $base_discount_amount string */
	protected $base_discount_amount;
	/** @var $base_total_taxes_and_charges string */
	protected $base_total_taxes_and_charges;
	/** @var $supplier_name string */
	protected $supplier_name;
	/** @var $items array */
	protected $items = [];
	/** @var $discount_amount string */
	protected $discount_amount;
	/** @var $name string */
	protected $name;
	/** @var $is_paid bool */
	protected $is_paid;
	/** @var $is_return bool */
	protected $is_return;
	/** @var $base_rounding_adjustment string */
	protected $base_rounding_adjustment;
	/** @var $write_off_amount string */
	protected $write_off_amount;
	/** @var $creation string */
	protected $creation;
	/** @var $party_account_currency string */
	protected $party_account_currency;
	/** @var $net_total string */
	protected $net_total;
	/** @var $owner string */
	protected $owner;
	/** @var $total_qty string */
	protected $total_qty;
	/** @var $posting_time string */
	protected $posting_time;
	/** @var $scan_barcode string */
	protected $scan_barcode;
	/** @var $price_list_currency string */
	protected $price_list_currency;
	/** @var $base_taxes_and_charges_added string */
	protected $base_taxes_and_charges_added;
	/** @var $supplier string */
	protected $supplier;
	/** @var $buying_price_list string */
	protected $buying_price_list;
	/** @var $is_opening bool */
	protected $is_opening;
	/** @var $allocate_advances_automatically int */
	protected $allocate_advances_automatically;
	/** @var $base_net_total string */
	protected $base_net_total;
	/** @var $taxes_and_charges_deducted string */
	protected $taxes_and_charges_deducted;
	/** @var $rounded_total string */
	protected $rounded_total;
	/** @var $apply_tds int */
	protected $apply_tds;
	/** @var $apply_discount_on string */
	protected $apply_discount_on;
	/** @var $in_words string */
	protected $in_words;
	/** @var $additional_discount_percentage string */
	protected $additional_discount_percentage;
	/** @var $disable_rounded_total int */
	protected $disable_rounded_total;
	/** @var $base_paid_amount string */
	protected $base_paid_amount;
	/** @var $conversion_rate string */
	protected $conversion_rate;
	/** @var $total_advance string */
	protected $total_advance;
	/** @var $total string */
	protected $total;
	/** @var $is_subcontracted bool */
	protected $is_subcontracted;
	/** @var $update_stock int */
	protected $update_stock;
	/** @var $base_total string */
	protected $base_total;
	/** @var $company string */
	protected $company;
	/** @var $language string */
	protected $language;
	/** @var $base_rounded_total string */
	protected $base_rounded_total;
	/** @var $tax_category string */
	protected $tax_category;
	/** @var $grand_total string */
	protected $grand_total;
	/** @var $idx int */
	protected $idx;
	/** @var $modified string */
	protected $modified;
	/** @var $rounding_adjustment string */
	protected $rounding_adjustment;
	/** @var $posting_date string */
	protected $posting_date;
	/** @var $against_expense_account string */
	protected $against_expense_account;
	/** @var $naming_series string */
	protected $naming_series;
	/** @var $is_proposed bool */
	protected $is_proposed;
	/** @var $currency string */
	protected $currency;
	/** @var $base_taxes_and_charges_deducted string */
	protected $base_taxes_and_charges_deducted;
	/** @var $on_hold int */
	protected $on_hold;
	/** @var $paid_amount string */
	protected $paid_amount;
	/** @var $credit_to string */
	protected $credit_to;
	/** @var $base_grand_total string */
	protected $base_grand_total;
	/** @var $docstatus int */
	protected $docstatus;
	/** @var $status string */
	protected $status;
	/** @var $group_same_items int */
	protected $group_same_items;
	/** @var $outstanding_amount string */
	protected $outstanding_amount;
	/** @var $total_net_weight string */
	protected $total_net_weight;
	/** @var $remarks string */
	protected $remarks;
	/** @var $plc_conversion_rate string */
	protected $plc_conversion_rate;
	/** @var $total_taxes_and_charges string */
	protected $total_taxes_and_charges;

	/**
	 * Constructor for the ERPNext Purchase Invoice model
	 * @param $data array The data array as retrieved from ERP Next
	 */
	public function __construct($data = null) {
		parent::__construct($data);
		if (!is_null($data) && isset($data->items)) {
			$this->items = [];
			foreach ($data->items as $item) {
				$this->items[] = new InvoiceItem($item);
			}
		}
	}

	public function __toString() {
		$data = $this->__toArray();
		$data["items"] = [];
		/** @var InvoiceItem $item */
		foreach ($this->items as $item) {
			$data["items"][] = $item->__toArray();
		}
		return json_encode($data, JSON_NUMERIC_CHECK);
	}

	/**
	 * @return string
	 */
	public function getBaseWriteOffAmount(): ?string {
		return $this->base_write_off_amount;
	}

	/**
	 * @param string $base_write_off_amount
	 */
	public function setBaseWriteOffAmount(string $base_write_off_amount) {
		$this->base_write_off_amount = $base_write_off_amount;
	}

	/**
	 * @return string
	 */
	public function getModifiedBy(): ?string {
		return $this->modified_by;
	}

	/**
	 * @param string $modified_by
	 */
	public function setModifiedBy(string $modified_by) {
		$this->modified_by = $modified_by;
	}

	/**
	 * @return string
	 */
	public function getTitle(): ?string {
		return $this->title;
	}

	/**
	 * @param string $title
	 */
	public function setTitle(string $title) {
		$this->title = $title;
	}

	/**
	 * @return int
	 */
	public function getSetPostingTime(): ?int {
		return $this->set_posting_time;
	}

	/**
	 * @param int $set_posting_time
	 */
	public function setSetPostingTime(int $set_posting_time) {
		$this->set_posting_time = $set_posting_time;
	}

	/**
	 * @return string
	 */
	public function getTaxesAndChargesAdded(): ?string {
		return $this->taxes_and_charges_added;
	}

	/**
	 * @param string $taxes_and_charges_added
	 */
	public function setTaxesAndChargesAdded(string $taxes_and_charges_added) {
		$this->taxes_and_charges_added = $taxes_and_charges_added;
	}

	/**
	 * @return string
	 */
	public function getBaseInWords(): ?string {
		return $this->base_in_words;
	}

	/**
	 * @param string $base_in_words
	 */
	public function setBaseInWords(string $base_in_words) {
		$this->base_in_words = $base_in_words;
	}

	/**
	 * @return string
	 */
	public function getDueDate(): ?string {
		return $this->due_date;
	}

	/**
	 * @param string $due_date
	 */
	public function setDueDate(string $due_date) {
		$this->due_date = $due_date;
	}

	/**
	 * @return string
	 */
	public function getDoctype(): ?string {
		return $this->doctype;
	}

	/**
	 * @param string $doctype
	 */
	public function setDoctype(string $doctype) {
		$this->doctype = $doctype;
	}

	/**
	 * @return int
	 */
	public function getIgnorePricingRule(): ?int {
		return $this->ignore_pricing_rule;
	}

	/**
	 * @param int $ignore_pricing_rule
	 */
	public function setIgnorePricingRule(int $ignore_pricing_rule) {
		$this->ignore_pricing_rule = $ignore_pricing_rule;
	}

	/**
	 * @return string
	 */
	public function getBaseDiscountAmount(): ?string {
		return $this->base_discount_amount;
	}

	/**
	 * @param string $base_discount_amount
	 */
	public function setBaseDiscountAmount(string $base_discount_amount) {
		$this->base_discount_amount = $base_discount_amount;
	}

	/**
	 * @return string
	 */
	public function getBaseTotalTaxesAndCharges(): ?string {
		return $this->base_total_taxes_and_charges;
	}

	/**
	 * @param string $base_total_taxes_and_charges
	 */
	public function setBaseTotalTaxesAndCharges(string $base_total_taxes_and_charges) {
		$this->base_total_taxes_and_charges = $base_total_taxes_and_charges;
	}

	/**
	 * @return string
	 */
	public function getSupplierName(): ?string {
		return $this->supplier_name;
	}

	/**
	 * @param string $supplier_name
	 */
	public function setSupplierName(string $supplier_name) {
		$this->supplier_name = $supplier_name;
	}

	/**
	 * @return array
	 */
	public function getItems(): array {
		return $this->items;
	}

	/**
	 * @param InvoiceItem $item
	 */
	public function addItem(InvoiceItem $item) {
		$this->items[] = $item;
	}

	/**
	 * @param array $items
	 */
	public function setItems(array $items) {
		$this->items = $items;
	}

	/**
	 * @return string
	 */
	public function getDiscountAmount(): ?string {
		return $this->discount_amount;
	}

	/**
	 * @param string $discount_amount
	 */
	public function setDiscountAmount(string $discount_amount) {
		$this->discount_amount = $discount_amount;
	}

	/**
	 * @return string
	 */
	public function getName(): ?string {
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName(string $name) {
		$this->name = $name;
	}

	/**
	 * @return bool
	 */
	public function isPaid(): bool {
		return $this->is_paid;
	}

	/**
	 * @param bool $is_paid
	 */
	public function setIsPaid(bool $is_paid) {
		$this->is_paid = $is_paid;
	}

	/**
	 * @return bool
	 */
	public function isReturn(): bool {
		return $this->is_return;
	}

	/**
	 * @param bool $is_return
	 */
	public function setIsReturn(bool $is_return) {
		$this->is_return = $is_return;
	}

	/**
	 * @return string
	 */
	public function getBaseRoundingAdjustment(): ?string {
		return $this->base_rounding_adjustment;
	}

	/**
	 * @param string $base_rounding_adjustment
	 */
	public function setBaseRoundingAdjustment(string $base_rounding_adjustment) {
		$this->base_rounding_adjustment = $base_rounding_adjustment;
	}

	/**
	 * @return string
	 */
	public function getWriteOffAmount(): ?string {
		return $this->write_off_amount;
	}

	/**
	 * @param string $write_off_amount
	 */
	public function setWriteOffAmount(string $write_off_amount) {
		$this->write_off_amount = $write_off_amount;
	}

	/**
	 * @return string
	 */
	public function getCreation(): ?string {
		return $this->creation;
	}

	/**
	 * @param string $creation
	 */
	public function setCreation(string $creation) {
		$this->creation = $creation;
	}

	/**
	 * @return string
	 */
	public function getPartyAccountCurrency(): ?string {
		return $this->party_account_currency;
	}

	/**
	 * @param string $party_account_currency
	 */
	public function setPartyAccountCurrency(string $party_account_currency) {
		$this->party_account_currency = $party_account_currency;
	}

	/**
	 * @return string
	 */
	public function getNetTotal(): ?string {
		return $this->net_total;
	}

	/**
	 * @param string $net_total
	 */
	public function setNetTotal(string $net_total) {
		$this->net_total = $net_total;
	}

	/**
	 * @return string
	 */
	public function getOwner(): ?string {
		return $this->owner;
	}

	/**
	 * @param string $owner
	 */
	public function setOwner(string $owner) {
		$this->owner = $owner;
	}

	/**
	 * @return string
	 */
	public function getTotalQty(): ?string {
		return $this->total_qty;
	}

	/**
	 * @param string $total_qty
	 */
	public function setTotalQty(string $total_qty) {
		$this->total_qty = $total_qty;
	}

	/**
	 * @return string
	 */
	public function getPostingTime(): ?string {
		return $this->posting_time;
	}

	/**
	 * @param string $posting_time
	 */
	public function setPostingTime(string $posting_time) {
		$this->posting_time = $posting_time;
	}

	/**
	 * @return string
	 */
	public function getScanBarcode(): ?string {
		return $this->scan_barcode;
	}

	/**
	 * @param string $scan_barcode
	 */
	public function setScanBarcode(string $scan_barcode) {
		$this->scan_barcode = $scan_barcode;
	}

	/**
	 * @return string
	 */
	public function getPriceListCurrency(): ?string {
		return $this->price_list_currency;
	}

	/**
	 * @param string $price_list_currency
	 */
	public function setPriceListCurrency(string $price_list_currency) {
		$this->price_list_currency = $price_list_currency;
	}

	/**
	 * @return string
	 */
	public function getBaseTaxesAndChargesAdded(): ?string {
		return $this->base_taxes_and_charges_added;
	}

	/**
	 * @param string $base_taxes_and_charges_added
	 */
	public function setBaseTaxesAndChargesAdded(string $base_taxes_and_charges_added) {
		$this->base_taxes_and_charges_added = $base_taxes_and_charges_added;
	}

	/**
	 * @return string
	 */
	public function getSupplier(): ?string {
		return $this->supplier;
	}

	/**
	 * @param string $supplier
	 */
	public function setSupplier(string $supplier) {
		$this->supplier = $supplier;
	}

	/**
	 * @return string
	 */
	public function getBuyingPriceList(): ?string {
		return $this->buying_price_list;
	}

	/**
	 * @param string $buying_price_list
	 */
	public function setBuyingPriceList(string $buying_price_list) {
		$this->buying_price_list = $buying_price_list;
	}

	/**
	 * @return bool
	 */
	public function isOpening(): bool {
		return $this->is_opening;
	}

	/**
	 * @param bool $is_opening
	 */
	public function setIsOpening(bool $is_opening) {
		$this->is_opening = $is_opening;
	}

	/**
	 * @return int
	 */
	public function getAllocateAdvancesAutomatically(): ?int {
		return $this->allocate_advances_automatically;
	}

	/**
	 * @param int $allocate_advances_automatically
	 */
	public function setAllocateAdvancesAutomatically(int $allocate_advances_automatically) {
		$this->allocate_advances_automatically = $allocate_advances_automatically;
	}

	/**
	 * @return string
	 */
	public function getBaseNetTotal(): ?string {
		return $this->base_net_total;
	}

	/**
	 * @param string $base_net_total
	 */
	public function setBaseNetTotal(string $base_net_total) {
		$this->base_net_total = $base_net_total;
	}

	/**
	 * @return string
	 */
	public function getTaxesAndChargesDeducted(): ?string {
		return $this->taxes_and_charges_deducted;
	}

	/**
	 * @param string $taxes_and_charges_deducted
	 */
	public function setTaxesAndChargesDeducted(string $taxes_and_charges_deducted) {
		$this->taxes_and_charges_deducted = $taxes_and_charges_deducted;
	}

	/**
	 * @return string
	 */
	public function getRoundedTotal(): ?string {
		return $this->rounded_total;
	}

	/**
	 * @param string $rounded_total
	 */
	public function setRoundedTotal(string $rounded_total) {
		$this->rounded_total = $rounded_total;
	}

	/**
	 * @return int
	 */
	public function getApplyTds(): ?int {
		return $this->apply_tds;
	}

	/**
	 * @param int $apply_tds
	 */
	public function setApplyTds(int $apply_tds) {
		$this->apply_tds = $apply_tds;
	}

	/**
	 * @return string
	 */
	public function getApplyDiscountOn(): ?string {
		return $this->apply_discount_on;
	}

	/**
	 * @param string $apply_discount_on
	 */
	public function setApplyDiscountOn(string $apply_discount_on) {
		$this->apply_discount_on = $apply_discount_on;
	}

	/**
	 * @return string
	 */
	public function getInWords(): ?string {
		return $this->in_words;
	}

	/**
	 * @param string $in_words
	 */
	public function setInWords(string $in_words) {
		$this->in_words = $in_words;
	}

	/**
	 * @return string
	 */
	public function getAdditionalDiscountPercentage(): ?string {
		return $this->additional_discount_percentage;
	}

	/**
	 * @param string $additional_discount_percentage
	 */
	public function setAdditionalDiscountPercentage(string $additional_discount_percentage) {
		$this->additional_discount_percentage = $additional_discount_percentage;
	}

	/**
	 * @return int
	 */
	public function getDisableRoundedTotal(): ?int {
		return $this->disable_rounded_total;
	}

	/**
	 * @param int $disable_rounded_total
	 */
	public function setDisableRoundedTotal(int $disable_rounded_total) {
		$this->disable_rounded_total = $disable_rounded_total;
	}

	/**
	 * @return string
	 */
	public function getBasePaidAmount(): ?string {
		return $this->base_paid_amount;
	}

	/**
	 * @param string $base_paid_amount
	 */
	public function setBasePaidAmount(string $base_paid_amount) {
		$this->base_paid_amount = $base_paid_amount;
	}

	/**
	 * @return string
	 */
	public function getConversionRate(): ?string {
		return $this->conversion_rate;
	}

	/**
	 * @param string $conversion_rate
	 */
	public function setConversionRate(string $conversion_rate) {
		$this->conversion_rate = $conversion_rate;
	}

	/**
	 * @return string
	 */
	public function getTotalAdvance(): ?string {
		return $this->total_advance;
	}

	/**
	 * @param string $total_advance
	 */
	public function setTotalAdvance(string $total_advance) {
		$this->total_advance = $total_advance;
	}

	/**
	 * @return string
	 */
	public function getTotal(): ?string {
		return $this->total;
	}

	/**
	 * @param string $total
	 */
	public function setTotal(string $total) {
		$this->total = $total;
	}

	/**
	 * @return bool
	 */
	public function isSubcontracted(): bool {
		return $this->is_subcontracted;
	}

	/**
	 * @param bool $is_subcontracted
	 */
	public function setIsSubcontracted(bool $is_subcontracted) {
		$this->is_subcontracted = $is_subcontracted;
	}

	/**
	 * @return int
	 */
	public function getUpdateStock(): ?int {
		return $this->update_stock;
	}

	/**
	 * @param int $update_stock
	 */
	public function setUpdateStock(int $update_stock) {
		$this->update_stock = $update_stock;
	}

	/**
	 * @return string
	 */
	public function getBaseTotal(): ?string {
		return $this->base_total;
	}

	/**
	 * @param string $base_total
	 */
	public function setBaseTotal(string $base_total) {
		$this->base_total = $base_total;
	}

	/**
	 * @return string
	 */
	public function getCompany(): ?string {
		return $this->company;
	}

	/**
	 * @param string $company
	 */
	public function setCompany(string $company) {
		$this->company = $company;
	}

	/**
	 * @return string
	 */
	public function getLanguage(): ?string {
		return $this->language;
	}

	/**
	 * @param string $language
	 */
	public function setLanguage(string $language) {
		$this->language = $language;
	}

	/**
	 * @return string
	 */
	public function getBaseRoundedTotal(): ?string {
		return $this->base_rounded_total;
	}

	/**
	 * @param string $base_rounded_total
	 */
	public function setBaseRoundedTotal(string $base_rounded_total) {
		$this->base_rounded_total = $base_rounded_total;
	}

	/**
	 * @return string
	 */
	public function getTaxCategory(): ?string {
		return $this->tax_category;
	}

	/**
	 * @param string $tax_category
	 */
	public function setTaxCategory(string $tax_category) {
		$this->tax_category = $tax_category;
	}

	/**
	 * @return string
	 */
	public function getGrandTotal(): ?string {
		return $this->grand_total;
	}

	/**
	 * @param string $grand_total
	 */
	public function setGrandTotal(string $grand_total) {
		$this->grand_total = $grand_total;
	}

	/**
	 * @return int
	 */
	public function getIdx(): ?int {
		return $this->idx;
	}

	/**
	 * @param int $idx
	 */
	public function setIdx(int $idx) {
		$this->idx = $idx;
	}

	/**
	 * @return string
	 */
	public function getModified(): ?string {
		return $this->modified;
	}

	/**
	 * @param string $modified
	 */
	public function setModified(string $modified) {
		$this->modified = $modified;
	}

	/**
	 * @return string
	 */
	public function getRoundingAdjustment(): ?string {
		return $this->rounding_adjustment;
	}

	/**
	 * @param string $rounding_adjustment
	 */
	public function setRoundingAdjustment(string $rounding_adjustment) {
		$this->rounding_adjustment = $rounding_adjustment;
	}

	/**
	 * @return string
	 */
	public function getPostingDate(): ?string {
		return $this->posting_date;
	}

	/**
	 * @param string $posting_date
	 */
	public function setPostingDate(string $posting_date) {
		$this->posting_date = $posting_date;
	}

	/**
	 * @return string
	 */
	public function getAgainstExpenseAccount(): ?string {
		return $this->against_expense_account;
	}

	/**
	 * @param string $against_expense_account
	 */
	public function setAgainstExpenseAccount(string $against_expense_account) {
		$this->against_expense_account = $against_expense_account;
	}

	/**
	 * @return string
	 */
	public function getNamingSeries(): ?string {
		return $this->naming_series;
	}

	/**
	 * @param string $naming_series
	 */
	public function setNamingSeries(string $naming_series) {
		$this->naming_series = $naming_series;
	}

	/**
	 * @return bool
	 */
	public function isProposed(): bool {
		return $this->is_proposed;
	}

	/**
	 * @param bool $is_proposed
	 */
	public function setIsProposed(bool $is_proposed) {
		$this->is_proposed = $is_proposed;
	}

	/**
	 * @return string
	 */
	public function getCurrency(): ?string {
		return $this->currency;
	}

	/**
	 * @param string $currency
	 */
	public function setCurrency(string $currency) {
		$this->currency = $currency;
	}

	/**
	 * @return string
	 */
	public function getBaseTaxesAndChargesDeducted(): ?string {
		return $this->base_taxes_and_charges_deducted;
	}

	/**
	 * @param string $base_taxes_and_charges_deducted
	 */
	public function setBaseTaxesAndChargesDeducted(string $base_taxes_and_charges_deducted) {
		$this->base_taxes_and_charges_deducted = $base_taxes_and_charges_deducted;
	}

	/**
	 * @return int
	 */
	public function getOnHold(): ?int {
		return $this->on_hold;
	}

	/**
	 * @param int $on_hold
	 */
	public function setOnHold(int $on_hold) {
		$this->on_hold = $on_hold;
	}

	/**
	 * @return string
	 */
	public function getPaidAmount(): ?string {
		return $this->paid_amount;
	}

	/**
	 * @param string $paid_amount
	 */
	public function setPaidAmount(string $paid_amount) {
		$this->paid_amount = $paid_amount;
	}

	/**
	 * @return string
	 */
	public function getCreditTo(): ?string {
		return $this->credit_to;
	}

	/**
	 * @param string $credit_to
	 */
	public function setCreditTo(string $credit_to) {
		$this->credit_to = $credit_to;
	}

	/**
	 * @return string
	 */
	public function getBaseGrandTotal(): ?string {
		return $this->base_grand_total;
	}

	/**
	 * @param string $base_grand_total
	 */
	public function setBaseGrandTotal(string $base_grand_total) {
		$this->base_grand_total = $base_grand_total;
	}

	/**
	 * @return int
	 */
	public function getDocstatus(): ?int {
		return $this->docstatus;
	}

	/**
	 * @param int $docstatus
	 */
	public function setDocstatus(int $docstatus) {
		$this->docstatus = $docstatus;
	}

	/**
	 * @return string
	 */
	public function getStatus(): ?string {
		return $this->status;
	}

	/**
	 * @param string $status
	 */
	public function setStatus(string $status) {
		$this->status = $status;
	}

	/**
	 * @return int
	 */
	public function getGroupSameItems(): ?int {
		return $this->group_same_items;
	}

	/**
	 * @param int $group_same_items
	 */
	public function setGroupSameItems(int $group_same_items) {
		$this->group_same_items = $group_same_items;
	}

	/**
	 * @return string
	 */
	public function getOutstandingAmount(): ?string {
		return $this->outstanding_amount;
	}

	/**
	 * @param string $outstanding_amount
	 */
	public function setOutstandingAmount(string $outstanding_amount) {
		$this->outstanding_amount = $outstanding_amount;
	}

	/**
	 * @return string
	 */
	public function getTotalNetWeight(): ?string {
		return $this->total_net_weight;
	}

	/**
	 * @param string $total_net_weight
	 */
	public function setTotalNetWeight(string $total_net_weight) {
		$this->total_net_weight = $total_net_weight;
	}

	/**
	 * @return string
	 */
	public function getRemarks(): ?string {
		return $this->remarks;
	}

	/**
	 * @param string $remarks
	 */
	public function setRemarks(string $remarks) {
		$this->remarks = $remarks;
	}

	/**
	 * @return string
	 */
	public function getPlcConversionRate(): ?string {
		return $this->plc_conversion_rate;
	}

	/**
	 * @param string $plc_conversion_rate
	 */
	public function setPlcConversionRate(string $plc_conversion_rate) {
		$this->plc_conversion_rate = $plc_conversion_rate;
	}

	/**
	 * @return string
	 */
	public function getTotalTaxesAndCharges(): ?string {
		return $this->total_taxes_and_charges;
	}

	/**
	 * @param string $total_taxes_and_charges
	 */
	public function setTotalTaxesAndCharges(string $total_taxes_and_charges) {
		$this->total_taxes_and_charges = $total_taxes_and_charges;
	}
}
