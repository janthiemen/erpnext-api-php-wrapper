# ERP Next PHP API wrapper
This is a simple API Wrapper for the ERP Next API. This project is primarily for personal use and currently only 
supports models and methods for the accounting module.

## Copyright and licensing
The ERPNext brand is copyrighted by Frappe technologies. This code is available under the GNU GNU General Public License
 (v3) that is included in the file LICENSE